package com.am.server.config.p6spy;

import cn.hutool.core.text.CharSequenceUtil;
import com.p6spy.engine.common.P6Util;
import com.p6spy.engine.logging.Category;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;

public class CustomSingleLineFormat implements MessageFormattingStrategy {
    /**
     * Formats a log message for the logging module
     *
     * @param connectionId the id of the connection
     * @param now          the current ime expressing in milliseconds
     * @param elapsed      the time in milliseconds that the operation took to complete
     * @param category     the category of the operation
     * @param prepared     the SQL statement with all bind variables replaced with actual values
     * @param sql          the sql statement executed
     * @param url          the database url where the sql statement executed
     * @return the formatted log message
     */
    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        if (CharSequenceUtil.equals(category, Category.STATEMENT.getName())) {
            return elapsed + " | " + P6Util.singleLine(sql);
        } else if (CharSequenceUtil.equals(category, Category.COMMIT.getName()) || CharSequenceUtil.equals(category, Category.ROLLBACK.getName())) {
            return category;
        }
        return now + "|" + elapsed + "|" + category + "|connection " + connectionId + "|url " + url + "|" + P6Util.singleLine(prepared) + "|" + P6Util.singleLine(sql);
    }
}
