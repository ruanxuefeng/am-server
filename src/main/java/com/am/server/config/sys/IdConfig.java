package com.am.server.config.sys;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 系统相关配置
 *
 * @author 阮雪峰
 */
@ConfigurationProperties(prefix = "sys.id")
@Configuration
@Getter
@Setter
public class IdConfig {
    /**
     * 终端ID
     */
    private Long workerId;
    /**
     * 数据中心ID
     */
    private Long dataCenterId;
}
