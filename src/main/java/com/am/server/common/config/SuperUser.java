package com.am.server.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
@EnableConfigurationProperties(SuperUser.class)
@ConfigurationProperties("common.super-user")
public class SuperUser {
    /**
     * 是否允许超级管理员登录
     */
    private Boolean enable = Boolean.FALSE;

    /**
     * 超级管理员主键
     */
    private Long id;

    /**
     * 超级管理员用户名
     */
    private String username;

    /**
     * DES3加密后的密码
     * 默认123456
     */
    private String password;
}
