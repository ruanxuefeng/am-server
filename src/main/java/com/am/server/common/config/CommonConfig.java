package com.am.server.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 阮雪峰
 */
@Configuration
@EnableConfigurationProperties(CommonConfig.class)
@ConfigurationProperties(prefix = "common")
@Setter
@Getter
public class CommonConfig {
    private String basePackage;

    private SuperUser superUser;
}
