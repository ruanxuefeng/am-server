package com.am.server.common.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 分页
 *
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Data
public class PageDto<T extends Serializable> implements Serializable {

    private static final int DEFAULT_PAGE_SIZE = 20;
    @Serial
    private static final long serialVersionUID = 2210045148101338343L;
    /**
     * 当前页
     */
    private Integer page;
    /**
     * 每页记录数
     */
    private Integer pageSize;
    /**
     * 总记录数
     */
    private Integer total;

    /**
     * 查询的数据
     */
    @JsonProperty("list")
    private List<T> rows;

    /**
     * 获取总页数
     */
    private Integer totalPage;

    /**
     * 获取开始查询的记录数
     *
     * @return int
     */
    @JsonIgnore
    public int getCol() {
        return (page - 1) * pageSize;
    }

    public PageDto() {
        pageSize = DEFAULT_PAGE_SIZE;
    }
}
