package com.am.server.common.base.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class PageQueryDto {
    /**
     * 要查询页数
     *
     * @required
     */
    private Integer page;

    /**
     * 每页记录数
     *
     * @required
     */
    private Integer pageSize;

    public PageQueryDto() {
        page = 1;
        pageSize = 20;
    }
}
