package com.am.server.common.base.model.dto;

import com.am.server.common.constant.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.MDC;

/**
 * 返回消息
 *
 * @author 阮雪峰
 */
@AllArgsConstructor
@Data
public class ResponseMessageDto {

    /**
     * 消息内容
     */
    private String message;

    /**
     * 请求唯一标识
     */
    private String traceId;

    public ResponseMessageDto(String message) {
        this();
        this.message = message;
    }

    public ResponseMessageDto() {
        traceId = MDC.get(Constant.TRACE_ID);
    }
}
