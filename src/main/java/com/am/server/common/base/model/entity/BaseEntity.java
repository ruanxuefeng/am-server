package com.am.server.common.base.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.time.LocalDateTime;

/**
 * 基本表结构
 *
 * @author 阮雪峰
 */
@Getter
@Setter
@MappedSuperclass
public class BaseEntity {
    /**
     * 主键
     */
    @Id
    private Long id;
    @Column(updatable = false)
    private LocalDateTime createdTime;

    @Column
    private LocalDateTime updatedTime;

    /**
     * 版本
     */
    @Version
    private Long revision;
}
