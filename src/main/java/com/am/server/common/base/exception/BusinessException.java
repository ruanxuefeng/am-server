package com.am.server.common.base.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * 业务异常<br>
 * 所有自定义运行时异常都需要继承此类
 */
@Getter
public class BusinessException extends RuntimeException {
    private final Integer code;
    
    public BusinessException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public BusinessException(String message, HttpStatus code) {
        super(message);
        this.code = code.value();
    }

    public BusinessException(String message, Throwable cause, Integer code) {
        super(message, cause);
        this.code = code;
    }

    public BusinessException(String message, Throwable cause, HttpStatus code) {
        super(message, cause);
        this.code = code.value();
    }
}
