package com.am.server.common.base.service;

import com.am.server.common.base.model.entity.BaseEntity;
import com.am.server.common.base.model.entity.EditableEntity;

/**
 * @author 阮雪峰
 */
public interface CommonService {

    /**
     * 获取登录用户id
     *
     * @return Long
     */
    Long getLoginUserId();

    /**
     * 当前用户是否是超级用户
     *
     * @return boolean
     */
    boolean isSupperUser();

    /**
     * 在新增或修改之前执行
     * 新增时会设置id、createdById、createdTime、updatedBy、updatedTime
     * 修改会更新updatedBy、updatedTime
     *
     * @param entity entity
     */
    void beforeSave(EditableEntity entity);

    /**
     * 在新增或修改之前执行
     * 新增时会设置id
     * 修改不会更新
     *
     * @param entity entity
     */
    void beforeSave(BaseEntity entity);

    /**
     * 生成一个新id
     *
     * @return Long
     */
    Long createNewId();
}
