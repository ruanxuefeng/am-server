package com.am.server.common.base.model.enumerate;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

/**
 * 性别
 *
 * @author 阮雪峰
 */
public enum Gender {
    /**
     * 男
     */
    MALE("男"),
    /**
     * 女
     */
    FEMALE("女");

    Gender(final String value) {
        this.value = value;
    }

    @Getter
    private final String value;

    @JsonCreator
    public static Gender create(final String code) {
        for (final Gender item : values()) {
            if (item.toString().equals(code)) {
                return item;
            }
        }
        return null;
    }
}
