package com.am.server.common.base.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 接受ID数组实体
 *
 * @author 阮雪峰
 */
@Setter
@Getter
public class IdsDto<T> {
    private List<T> ids;
}
