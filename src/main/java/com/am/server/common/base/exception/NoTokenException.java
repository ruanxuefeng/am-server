package com.am.server.common.base.exception;

/**
 * 没有token异常
 *
 * @author 阮雪峰
 */
public class NoTokenException extends RuntimeException {
}
