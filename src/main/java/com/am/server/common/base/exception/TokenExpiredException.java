package com.am.server.common.base.exception;

/**
 * token过期
 *
 * @author 阮雪峰
 */
public class TokenExpiredException extends RuntimeException {
}
