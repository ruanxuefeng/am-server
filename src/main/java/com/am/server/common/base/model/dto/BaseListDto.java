package com.am.server.common.base.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 公用列表字段
 *
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Setter
@Getter
public class BaseListDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 6432978471192662621L;
    /**
     * 主键
     */
    private Long id;

    /**
     * 创建人
     */
    private String creatorName;

    /**
     * 创建时间
     */
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdTime;

    /**
     * 乐观锁
     */
    private Long revision;
}
