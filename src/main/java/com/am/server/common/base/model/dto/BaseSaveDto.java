package com.am.server.common.base.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class BaseSaveDto {
    /**
     * 乐观锁
     *
     * @required
     */
    private Long revision;
}
