package com.am.server.common.base.model.entity;

import com.am.server.api.system.user.model.entity.AdminUserEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * 所有DO实体的父类
 *
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Getter
@Setter
@MappedSuperclass
public class EditableEntity extends BaseEntity {

    @Column(name = "created_by")
    private Long createdById;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "created_by", insertable = false, updatable = false)
    private AdminUserEntity createdBy;

    @Column(name = "updated_by")
    private Long updatedById;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "updated_by", insertable = false, updatable = false)
    private AdminUserEntity updatedBy;
}


