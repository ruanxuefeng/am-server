package com.am.server.common.util;

import com.am.server.common.base.model.dto.ResponseMessageDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 公共相应工具类
 *
 * @author 阮雪峰
 */
public class ResponseUtils {
    private ResponseUtils() {
    }

    /**
     * 成功响应
     *
     * @param body 响应数据
     * @param <T>  响应数据
     * @return ResponseEntity<T>
     */
    public static <T> ResponseEntity<T> ok(T body) {
        return ResponseEntity.ok(body);
    }

    /**
     * 成功响应
     *
     * @param body 响应消息
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> ok(String body) {
        return ResponseEntity.ok(new ResponseMessageDto(body));
    }

    /**
     * bad request 响应
     *
     * @param body 响应数据
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> badRequest(String body) {
        return ResponseEntity.badRequest().body(new ResponseMessageDto(body));
    }

    /**
     * 未授权
     *
     * @param body body
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> unauthorized(String body) {
        return new ResponseEntity<>(new ResponseMessageDto(body), HttpStatus.UNAUTHORIZED);
    }

    /**
     * @param body body
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> notAcceptable(String body) {
        return new ResponseEntity<>(new ResponseMessageDto(body), HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * @param body body
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> preconditionFailed(String body) {
        return new ResponseEntity<>(new ResponseMessageDto(body), HttpStatus.PRECONDITION_FAILED);
    }

    /**
     * @param body body
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> forbidden(String body) {
        return new ResponseEntity<>(new ResponseMessageDto(body), HttpStatus.FORBIDDEN);
    }

    /**
     * @param body body
     * @return ResponseEntity<ResponseMessageDto>
     */
    public static ResponseEntity<ResponseMessageDto> serverError(String body) {
        return new ResponseEntity<>(new ResponseMessageDto(body), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<ResponseMessageDto> extendResponse(int code, String body) {
        return ResponseEntity.status(code).body(new ResponseMessageDto(body));
    }
}
