package com.am.server.common.util.exception;

import java.io.Serial;

/**
 * @author 阮雪峰
 */
public class NotFoundIndexParameterException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 6505858372127568845L;

    public NotFoundIndexParameterException(final int maxParameterIndex, final int customParameterIndex) {
        super(String.format("最大参数索引：%s，传入参数索引：%s", maxParameterIndex, customParameterIndex));
    }
}
