package com.am.server.common.util.exception;

/**
 * @author 阮雪峰
 */
public class NotFoundParameterException extends RuntimeException {
    public NotFoundParameterException(String parameterName) {
        super(String.format("没有找到名为%s的参数", parameterName));
    }
}
