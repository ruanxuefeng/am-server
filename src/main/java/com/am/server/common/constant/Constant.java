package com.am.server.common.constant;

/**
 * 常量
 *
 * @author 阮雪峰
 */
public class Constant {
    /**
     * 初始密码
     */
    public static final String INITIAL_PASSWORD = "123456";

    /**
     * 根目录
     */
    public static final String ROOT = "/api";
    public static final String ADMIN_ROOT = ROOT + "/admin";
    public static final String TRACE_ID = "traceId";
    public static final String TOKEN = "AM-TOKEN";


    private Constant() {
    }
}
