package com.am.server.common.constant;

/**
 * WebSocket路径
 *
 * @author 阮雪峰
 */
public class WebSocketUriConstant {
    private WebSocketUriConstant() {
    }

    /**
     * 发送权限角色被修改通知
     * %s: 用户id
     */
    public static final String NOTICE_UPDATE = "/topic/permission/%s";
}
