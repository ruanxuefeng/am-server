package com.am.server.common.constant;

/**
 * Spring Data JPA 查询常量
 *
 * @author 阮雪峰
 */
public class QueryConstant {
    private QueryConstant() {
    }

    public static final String FULL_LIKE_QUERY_FORMAT = "%%%s%%";
}
