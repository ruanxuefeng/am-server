package com.am.server.common.constant;

/**
 * @author 阮雪峰
 */
public class MessageConstant {
    private MessageConstant() {
    }

    /**
     * 成功
     */
    public static final String SUCCESS = "成功";
    /**
     * 新增成功
     */
    public static final String SAVE_SUCCESS = "保存成功";
    /**
     * 修改成功
     */
    public static final String UPDATE_SUCCESS = "修改成功";
    /**
     * 删除成功
     */
    public static final String DELETE_SUCCESS = "删除成功";
    /**
     * 缺少删除主键
     */
    public static final String COMMON_UPDATE_PRIMARY_KEY_NULL = "请选择要修改的数据";
    /**
     * 缺少删除主键
     */
    public static final String COMMON_DELETE_PRIMARY_KEY_NULL = "请选择要删除的数据";
    /**
     * 缺少操作主键
     */
    public static final String COMMON_OPERATE_PRIMARY_KEY_NULL = "请选择要操作的数据";
    /**
     * 方法参数类型错误
     */
    public static final String PARAMETER_TYPE_ERROR = "参数转换异常";
    /**
     * 请求没有携带token，提示登录
     */
    public static final String NO_TOKEN = "请先登录";
    /**
     * 管理员没有为您分配任何权限，请联系管理员
     */
    public static final String NO_PERMISSION = "管理员没有为您分配任何权限，请联系管理员";
    /**
     * 用户名或密码不正确
     */
    public static final String VALIDATE_FAIL = "用户名或密码不正确";
    /**
     * token过期
     */
    public static final String TOKEN_EXPIRED = "登录过期，请重新登录";
    /**
     * 没有权限访问
     */
    public static final String NO_PERMISSION_ACCESS = "不好意思，您没有访问权限";
    /**
     * 文件上传失败
     */
    public static final String FILE_UPLOAD_FAIL = "文件上传失败";
    /**
     * 服务器异常
     */
    public static final String SERVER_ERROR = "服务器异常";
}
