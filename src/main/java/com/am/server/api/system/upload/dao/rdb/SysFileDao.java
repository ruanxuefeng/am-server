package com.am.server.api.system.upload.dao.rdb;

import com.am.server.api.system.upload.model.entity.SysFileEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface SysFileDao {
    /**
     * save
     *
     * @param sysFile sysFile
     */
    void save(SysFileEntity sysFile);

    /**
     * 删除
     *
     * @param entity entity
     */
    void delete(SysFileEntity entity);

    /**
     * 根据id查询
     *
     * @param id id
     * @return Optional<SysFileEntity>
     */
    Optional<SysFileEntity> findById(Long id);

    /**
     * 删除所有
     *
     * @param entities entities
     */
    void deleteAll(List<SysFileEntity> entities);
}
