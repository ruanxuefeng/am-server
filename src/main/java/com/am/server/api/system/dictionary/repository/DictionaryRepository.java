package com.am.server.api.system.dictionary.repository;

import com.am.server.api.system.dictionary.model.entity.DictionaryEntity;
import com.am.server.common.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Repository
public interface DictionaryRepository extends BaseRepository<DictionaryEntity> {
    /**
     * 根据code查询
     *
     * @param code code
     * @return Optional<DictionaryEntity>
     */
    Optional<DictionaryEntity> findFirstByCode(String code);

    /**
     * 排除当前数据，判断编码是否存在
     *
     * @param code code
     * @param id   id
     * @return Optional<DictionaryEntity>
     */
    Optional<DictionaryEntity> findFirstByCodeAndIdNot(String code, Long id);
}
