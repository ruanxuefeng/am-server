package com.am.server.api.system.permission.listener;

import com.am.server.api.system.permission.service.SystemPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @author 阮雪峰
 */
@Slf4j
@Component
public class LoadPermissionListener implements ApplicationListener<ContextRefreshedEvent> {

    private final SystemPermissionService systemPermissionService;

    public LoadPermissionListener(SystemPermissionService systemPermissionService) {
        this.systemPermissionService = systemPermissionService;
    }

    @Override
    public void onApplicationEvent(@NotNull ContextRefreshedEvent event) {
        systemPermissionService.loadPermissionToCache();
    }
}
