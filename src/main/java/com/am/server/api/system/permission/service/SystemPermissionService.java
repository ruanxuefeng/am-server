package com.am.server.api.system.permission.service;

import com.am.server.api.system.permission.config.model.Menu;

import java.util.List;

/**
 * @author 阮雪峰
 */
public interface SystemPermissionService {
    /**
     * 将权限加载至缓存
     */
    void loadPermissionToCache();

    /**
     * 查询所有权限，包括按钮
     *
     * @return List<Menu>
     */
    List<Menu> getAllTree();

    /**
     * 查询所有的菜单，不包括按钮
     *
     * @return List<Menu>
     */
    List<Menu> getMenuTree();

    /**
     * 获取所有权限标识
     *
     * @return List<String>
     */
    List<Menu> getMenuList();

    /**
     * 所有的key
     *
     * @return List<String>
     */
    List<String> getAllKeyList();
}
