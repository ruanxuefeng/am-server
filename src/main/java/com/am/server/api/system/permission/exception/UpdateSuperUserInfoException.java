package com.am.server.api.system.permission.exception;

import com.am.server.common.base.exception.BusinessException;
import org.springframework.http.HttpStatus;

/**
 * @author 阮雪峰
 */
public class UpdateSuperUserInfoException extends BusinessException {
    public UpdateSuperUserInfoException() {
        super("不能修改超级管理员用户信息", HttpStatus.NOT_ACCEPTABLE);
    }
}
