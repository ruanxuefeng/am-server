package com.am.server.api.system.parameter.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.am.server.api.system.log.aspect.annotation.WriteLog;
import com.am.server.api.system.parameter.constant.ParameterLogConstant;
import com.am.server.api.system.parameter.model.dto.*;
import com.am.server.api.system.parameter.service.ParameterService;
import com.am.server.common.base.model.dto.ResponseMessageDto;
import com.am.server.common.constant.Constant;
import com.am.server.common.constant.MessageConstant;
import com.am.server.common.util.ResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统参数
 *
 * @author 阮雪峰
 */
@WriteLog(ParameterLogConstant.MENU_NAME)
@SaCheckLogin
@RestController
@RequestMapping(Constant.ADMIN_ROOT + "/parameter")
public class ParameterController {

    private final ParameterService parameterService;

    public ParameterController(final ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    /**
     * 树形参数查询
     *
     * @return ResponseEntity<List < ParameterTreeVo>>
     */
    @GetMapping("/treeList")
    public ResponseEntity<List<ParameterTreeDto>> treeList(ParameterListQueryDto query) {
        return ResponseEntity.ok(parameterService.treeList(query));
    }

    /**
     * 更新参数值
     *
     * @param dto dto
     * @return ResponseEntity<MessageVO>
     */
    @WriteLog(ParameterLogConstant.PARAMETER_UPDATE_METHOD_NAME)
    @PostMapping("/update")
    public ResponseEntity<ResponseMessageDto> update(@RequestBody UpdateParameterDto dto) {
        parameterService.update(dto);
        return ResponseUtils.ok(MessageConstant.UPDATE_SUCCESS);
    }

    /**
     * 根据uniqueKey查询参数
     *
     * @param dto dto
     * @return ResponseEntity<ParameterVo>
     */
    @GetMapping("/getByUniqueKey")
    public ResponseEntity<ParameterDto> getByUniqueKey(UniqueKeyDto dto) {
        return ResponseUtils.ok(parameterService.getByUniqueKey(dto.getUniqueKey()));
    }

    /**
     * 根据uniqueKey查询参数
     *
     * @param dto dto
     * @return ResponseEntity<ParameterVo>
     */
    @GetMapping("/getByUniqueKeys")
    public ResponseEntity<List<ParameterDto>> getByUniqueKeys(UniqueKeysDto dto) {
        return ResponseUtils.ok(parameterService.getByUniqueKeys(dto.getUniqueKeys()));
    }
}
