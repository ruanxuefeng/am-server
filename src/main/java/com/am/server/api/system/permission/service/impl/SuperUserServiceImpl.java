package com.am.server.api.system.permission.service.impl;

import com.am.server.api.system.permission.service.SuperUserService;
import com.am.server.common.config.CommonConfig;
import com.am.server.common.config.SuperUser;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Service
public class SuperUserServiceImpl implements SuperUserService {

    private final CommonConfig commonConfig;

    public SuperUserServiceImpl(CommonConfig commonConfig) {
        this.commonConfig = commonConfig;
    }


    @Override
    public SuperUser get() {
        return Optional.ofNullable(commonConfig.getSuperUser()).orElseGet(() -> {
            SuperUser superUser = new SuperUser();
            superUser.setEnable(Boolean.FALSE);
            return superUser;
        });
    }
}
