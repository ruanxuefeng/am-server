package com.am.server.api.system.upload.service;

import com.am.server.api.system.upload.enumerate.FileType;
import com.am.server.api.system.upload.model.dto.UploadResultDto;
import com.am.server.api.system.upload.model.entity.SysFileEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 系统文件
 *
 * @author 阮雪峰
 */
@Service
public interface SysFileService {
    /**
     * 保存系统文件
     *
     * @param file 上传的文件
     * @param type 文件类型
     * @return SysFile
     */
    UploadResultDto save(MultipartFile file, FileType type);

    /**
     * 修改文件内容
     *
     * @param file    用户上传的新闻文件
     * @param sysFile 本地原来的文件
     * @param type    type
     */
    String updateFileContent(MultipartFile file, SysFileEntity sysFile, FileType type);

    /**
     * 删除
     *
     * @param entity entity
     */
    void remove(SysFileEntity entity);

    /**
     * 删除
     *
     * @param id id
     */
    void remove(Long id);

    /**
     * 删除全部
     *
     * @param entities entities
     */
    void removeAll(List<SysFileEntity> entities);

    /**
     * 根据id查询
     *
     * @param id id
     * @return SysFileEntity
     */
    SysFileEntity findById(Long id);
}
