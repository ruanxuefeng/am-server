package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.model.enumerate.Gender;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 保存、修改
 *
 * @author 阮雪峰
 */
@Data
public class SaveAdminUserDto {

    /**
     * 用户名
     */
    @NotBlank(message = "user.username.blank")
    @Length(max = 64, message = "user.username.long")
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Gender gender;
}
