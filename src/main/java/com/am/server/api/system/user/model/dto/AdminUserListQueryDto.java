package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.model.dto.PageQueryDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 列表查询条件
 *
 * @author 阮雪峰
 */
@Setter
@Getter
public class AdminUserListQueryDto extends PageQueryDto {

    /**
     * 姓名
     */
    private String name;

    /**
     * 用户名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;
}
