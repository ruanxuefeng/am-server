package com.am.server.api.system.permission.service;

import com.am.server.common.config.SuperUser;

/**
 * @author 阮雪峰
 */
public interface SuperUserService {
    /**
     * 获取当前超级管理员
     *
     * @return SuperUserDo
     */
    SuperUser get();
}
