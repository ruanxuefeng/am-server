package com.am.server.api.system.role.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author 阮雪峰
 */
@Data
public class UpdateRoleDto {
    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    @NotBlank(message = "role.name.blank")
    @Length(min = 1, max = 20, message = "role.name.length")
    private String name;

    /**
     * 主页id
     */
    @NotBlank(message = "请选择主页")
    private String homePageId;

    /**
     * 备注
     */
    private String memo;
}
