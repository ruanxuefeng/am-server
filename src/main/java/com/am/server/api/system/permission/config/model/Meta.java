package com.am.server.api.system.permission.config.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Setter
@Getter
public class Meta {
    private String title;
    private String icon;
    private Boolean ignoreAuth;
    private Boolean affix;
}
