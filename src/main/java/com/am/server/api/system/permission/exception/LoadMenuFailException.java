package com.am.server.api.system.permission.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 阮雪峰
 */
@Slf4j
public class LoadMenuFailException extends RuntimeException {
    public LoadMenuFailException(Exception e) {
        super("加载权限配置文件失败！请确认permission.yml文件是否存在，并且在resources目录下！", e);
    }
}
