package com.am.server.api.system.log.controller;

import com.am.server.api.system.log.model.dto.LogDto;
import com.am.server.api.system.log.model.dto.LogListQueryDto;
import com.am.server.api.system.log.service.LogService;
import com.am.server.common.base.model.dto.PageDto;
import com.am.server.common.constant.Constant;
import com.am.server.common.util.ResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 日志
 */
@RestController
@RequestMapping(Constant.ADMIN_ROOT + "/log")
public class LogController {
    private final LogService logService;

    public LogController(final LogService logService) {
        this.logService = logService;
    }

    /**
     * 列表查询
     *
     * @param query query
     * @return org.springframework.http.ResponseEntity
     */
    @GetMapping("find")
    public ResponseEntity<PageDto<LogDto>> find(LogListQueryDto query) {
        return ResponseUtils.ok(logService.find(query));
    }
}
