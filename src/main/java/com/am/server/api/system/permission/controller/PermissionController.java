package com.am.server.api.system.permission.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.am.server.api.system.log.aspect.annotation.WriteLog;
import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.api.system.permission.constant.PermissionLogConstant;
import com.am.server.api.system.permission.model.dto.MenuTreeDto;
import com.am.server.api.system.permission.service.PermissionService;
import com.am.server.api.system.role.constant.RolePermissionConstant;
import com.am.server.common.base.model.dto.IdsDto;
import com.am.server.common.constant.Constant;
import com.am.server.common.util.ResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 权限菜单
 *
 * @author 阮雪锋
 */
@SaCheckLogin
@WriteLog(PermissionLogConstant.MENU_NAME)
@RestController
@RequestMapping(Constant.ADMIN_ROOT + "/permission")
public class PermissionController {


    private final PermissionService permissionService;

    public PermissionController(final PermissionService permissionService) {
        this.permissionService = permissionService;
    }


    /**
     * 查询权限树
     *
     * @return ResponseEntity
     */
    @SaCheckPermission(RolePermissionConstant.ROLE_PERMISSION_AUTHORIZE)
    @GetMapping("/tree")
    public ResponseEntity<List<MenuTreeDto>> tree() {
        return ResponseUtils.ok(permissionService.tree());
    }

    /**
     * 查询权限树
     *
     * @return ResponseEntity
     */
    @GetMapping("/menu/tree")
    public ResponseEntity<List<MenuTreeDto>> menuTree() {
        return ResponseUtils.ok(permissionService.menuTree());
    }

    /**
     * 根据id查询菜单
     *
     * @param dto dto
     * @return ResponseEntity<Menu>
     */
    @PostMapping("/menu/findByIds")
    public ResponseEntity<List<Menu>> findById(@RequestBody IdsDto<String> dto) {
        return ResponseUtils.ok(permissionService.findByIds(dto.getIds()));
    }
}
