package com.am.server.api.system.dictionary.dao.rdb;

import com.am.server.api.system.dictionary.model.dto.DictionaryItemListQueryDto;
import com.am.server.api.system.dictionary.model.entity.DictionaryItemEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface DictionaryItemDao {
    List<DictionaryItemEntity> find(DictionaryItemListQueryDto dto);

    /**
     * 保存
     *
     * @param entity entity
     */
    void save(DictionaryItemEntity entity);

    /**
     * 删除
     *
     * @param id id
     */
    void delete(Long id);

    /**
     * 根据id查询
     *
     * @param id id
     * @return Optional<DictionaryItemEntity>
     */
    Optional<DictionaryItemEntity> findById(Long id);
}
