package com.am.server.api.system.dictionary.constant;

/**
 * @author 阮雪峰
 */
public class DictionaryLogConstant {
    private DictionaryLogConstant() {
    }

    public static final String MENU_NAME = "字典管理";
}
