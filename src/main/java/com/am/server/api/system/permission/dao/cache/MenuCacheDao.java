package com.am.server.api.system.permission.dao.cache;

import com.am.server.api.system.permission.config.model.Menu;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface MenuCacheDao {
    /**
     * 保存所有菜单
     *
     * @param list list
     */
    void saveAll(List<Menu> list);

    /**
     * 根据id查询
     *
     * @param id id
     * @return Optional<Menu>
     */
    Optional<Menu> findById(String id);

    /**
     * 根据id查询
     *
     * @param ids ids
     * @return List<Menu>
     */
    List<Menu> findByIds(List<String> ids);
}
