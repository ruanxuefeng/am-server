package com.am.server.api.system.role.constant;

/**
 * 权限常量
 *
 * @author 阮雪峰
 */
public class RolePermissionConstant {
    /**
     * 新增
     */
    public static final String ROLE_PERMISSION_ADD = "system-role-add";
    /**
     * 修改
     */
    public static final String ROLE_PERMISSION_UPDATE = "system-role-update";
    /**
     * 删除
     */
    public static final String ROLE_PERMISSION_DELETE = "system-role-delete";
    /**
     * 授权
     */
    public static final String ROLE_PERMISSION_AUTHORIZE = "system-role-authorize";

    private RolePermissionConstant() {
    }
}
