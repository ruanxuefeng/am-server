package com.am.server.api.system.dictionary.service;

import com.am.server.api.system.dictionary.model.dto.DictionaryItemDto;
import com.am.server.api.system.dictionary.model.dto.DictionaryItemListQueryDto;
import com.am.server.api.system.dictionary.model.dto.SaveDictionaryItemDto;
import com.am.server.api.system.dictionary.model.dto.UpdateDictionaryItemDto;

import java.util.List;

/**
 * @author 阮雪峰
 */
public interface DictionaryItemService {
    /**
     * 查询不分页
     *
     * @param dto dto
     * @return List<DictionaryItemDto>
     */
    List<DictionaryItemDto> find(DictionaryItemListQueryDto dto);

    /**
     * 新增
     *
     * @param dto dto
     */
    void save(SaveDictionaryItemDto dto);

    /**
     * 修改
     *
     * @param dto dto
     */
    void update(UpdateDictionaryItemDto dto);

    /**
     * 删除
     *
     * @param id id
     */
    void delete(Long id);
}
