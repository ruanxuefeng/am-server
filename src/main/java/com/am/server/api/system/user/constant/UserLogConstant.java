package com.am.server.api.system.user.constant;

/**
 * @author 阮雪峰
 */
public class UserLogConstant {
    private UserLogConstant() {
    }

    public static final String MENU_NAME = "用户管理";

    public static final String USER_ADD_METHOD_NAME = "新增";
    public static final String USER_UPDATE_METHOD_NAME = "修改";
    public static final String USER_DELETE_METHOD_NAME = "删除";
    public static final String USER_CHANGE_ROLE_METHOD_NAME = "修改角色";

    public static final String USER_LOGIN = "登录";
}
