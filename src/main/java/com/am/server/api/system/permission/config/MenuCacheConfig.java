package com.am.server.api.system.permission.config;

import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.config.cache.RedisConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;

/**
 * @author 阮雪峰
 */
@Component
public class MenuCacheConfig {
    /**
     * 菜单缓存
     *
     * @param redisConnectionFactory redisConnectionFactory
     * @return RedisTemplate<String, Menu>
     */
    @Bean("menuCacheRedisTemplate")
    public RedisTemplate<String, Menu> menuCacheRedisTemplate(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<String, Menu> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
        Jackson2JsonRedisSerializer<Menu> serializer = new Jackson2JsonRedisSerializer<>(Menu.class);

        return RedisConfig.configTemplate(template, serializer);
    }
}
