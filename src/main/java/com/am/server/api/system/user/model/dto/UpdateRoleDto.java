package com.am.server.api.system.user.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 修改角色
 *
 * @author 阮雪峰
 */
@Data
public class UpdateRoleDto {

    /**
     * 用户主键
     *
     * @required
     */
    @NotNull(message = "common.operate.primaryKey.null")
    private List<Long> ids;

    /**
     * 角色主键数组
     *
     * @required
     */
    private List<Long> roleIdList;

    /**
     * 主页菜单id
     *
     * @required
     */
    private String homePageId;
}
