package com.am.server.api.system.dictionary.model.dto;

import com.am.server.common.constant.MessageConstant;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class UpdateDictionaryItemDto {
    @NotNull(message = MessageConstant.COMMON_UPDATE_PRIMARY_KEY_NULL)
    private Long id;
    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    private String name;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 备注
     */
    private String memo;
    /**
     * 对应字典id
     */
    private Long dictionaryId;
}
