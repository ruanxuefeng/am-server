package com.am.server.api.system.log.model.mapper;

import com.am.server.api.system.log.model.dto.LogDto;
import com.am.server.api.system.log.model.entity.LogEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * @author 阮雪峰
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface LogMapper {
    LogDto toDto(LogEntity entity);
}
