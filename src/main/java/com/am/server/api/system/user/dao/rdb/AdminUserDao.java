package com.am.server.api.system.user.dao.rdb;

import com.am.server.api.system.user.model.dto.AdminUserListQueryDto;
import com.am.server.api.system.user.model.entity.AdminUserEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface AdminUserDao {
    /**
     * 邮箱查询用户
     *
     * @param email email
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByEmail(String email);

    /**
     * 在修改的时候排除现有用户进行是否存在校验
     *
     * @param id    id
     * @param email email
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByIdNotAndEmail(Long id, String email);

    /**
     * 用户名查询用户
     *
     * @param username 用户名
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByUsername(String username);

    /**
     * 在修改的时候排除现有用户进行是否存在校验
     *
     * @param id       id
     * @param username username
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByIdNotAndUsername(Long id, String username);

    /**
     * 通过主键查询
     *
     * @param id 主键
     * @return Optional<AdminUserDo>
     */
    Optional<AdminUserEntity> findById(Long id);

    /**
     * 分页查询
     *
     * @param listQuery 查询条件
     * @return Page<AdminUserDo>
     */
    Page<AdminUserEntity> findAll(AdminUserListQueryDto listQuery);

    /**
     * 保存
     *
     * @param user user
     */
    void save(AdminUserEntity user);

    /**
     * delete by id
     *
     * @param id 主键
     */
    void deleteById(Long id);

    /**
     * 根据id查询
     *
     * @param ids ids
     * @return List<AdminUserEntity>
     */
    List<AdminUserEntity> findAllById(List<Long> ids);

    /**
     * 保存全部
     *
     * @param users users
     */
    void saveAll(List<AdminUserEntity> users);
}
