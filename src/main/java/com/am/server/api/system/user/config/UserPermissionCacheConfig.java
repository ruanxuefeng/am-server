package com.am.server.api.system.user.config;

import com.am.server.api.system.user.model.entity.UserPermissionCacheEntity;
import com.am.server.config.cache.RedisConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;

/**
 * @author 阮雪峰
 */
@Component
public class UserPermissionCacheConfig {
    /**
     * 用户权限缓存
     *
     * @param redisConnectionFactory redisConnectionFactory
     * @return RedisTemplate<String, UserPermissionDo>
     */
    @Bean("userPermissionCacheRedisTemplate")
    public RedisTemplate<String, UserPermissionCacheEntity> userPermissionCacheRedisTemplate(final RedisConnectionFactory redisConnectionFactory) {
        final RedisTemplate<String, UserPermissionCacheEntity> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
        final Jackson2JsonRedisSerializer<UserPermissionCacheEntity> serializer = new Jackson2JsonRedisSerializer<>(UserPermissionCacheEntity.class);

        return RedisConfig.configTemplate(template, serializer);
    }
}
