package com.am.server.api.system.parameter.model.entity;

import com.am.server.api.system.parameter.model.enumerate.ParameterType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Getter
@Setter
public class ParameterCacheEntity {
    /**
     * 主键
     */
    private Long id;
    /**
     * 参数值
     */
    private String value;
    /**
     * 参数名称
     */
    private String name;
    /**
     * 唯一key值
     */
    private String uniqueKey;
    /**
     * 类型
     */
    private ParameterType type;
    /**
     * 参数目录
     */
    private String catlog;
    /**
     * 参数备注
     */
    private String description;
    /**
     * 全名
     */
    private String fullPath;
}
