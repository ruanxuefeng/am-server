package com.am.server.api.system.permission.dao.cache.impl;

import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.api.system.permission.dao.cache.MenuCacheDao;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Repository
public class MenuCacheDaoImpl implements MenuCacheDao {
    /**
     * 菜单缓存key模板
     */
    private static final String KEY_TEMPLATE = "system: menu: %s";
    private final RedisTemplate<String, Menu> menuCacheRedisTemplate;

    public MenuCacheDaoImpl(RedisTemplate<String, Menu> menuCacheRedisTemplate) {
        this.menuCacheRedisTemplate = menuCacheRedisTemplate;
    }

    @Override
    public void saveAll(List<Menu> list) {
        list.forEach(item -> menuCacheRedisTemplate.opsForValue().set(String.format(KEY_TEMPLATE, item.getId()), item));
    }

    @Override
    public Optional<Menu> findById(String id) {
        return Optional.ofNullable(menuCacheRedisTemplate.opsForValue().get(String.format(KEY_TEMPLATE, id)));
    }

    @Override
    public List<Menu> findByIds(List<String> ids) {
        return ids.stream().map(id -> menuCacheRedisTemplate.opsForValue().get(String.format(KEY_TEMPLATE, id))).filter(Objects::nonNull).toList();
    }

}
