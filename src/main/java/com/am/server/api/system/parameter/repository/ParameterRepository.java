package com.am.server.api.system.parameter.repository;

import com.am.server.api.system.parameter.model.entity.ParameterEntity;
import com.am.server.common.base.repository.BaseRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface ParameterRepository extends BaseRepository<ParameterEntity> {
    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKey uniqueKey
     * @return ParameterEntity
     */
    Optional<ParameterEntity> findByUniqueKey(String uniqueKey);

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKeys uniqueKeys
     * @return List<ParameterEntity>
     */
    List<ParameterEntity> findAllByUniqueKeyIn(List<String> uniqueKeys);
}
