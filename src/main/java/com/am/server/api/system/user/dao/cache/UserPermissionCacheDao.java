package com.am.server.api.system.user.dao.cache;

import com.am.server.api.system.user.model.entity.UserPermissionCacheEntity;

import java.util.List;

/**
 * @author 阮雪峰
 */

public interface UserPermissionCacheDao {

    /**
     * 查询用户权限缓存
     *
     * @param uid 用户主键
     * @return UserPermissionDO
     */
    UserPermissionCacheEntity findById(Long uid);

    /**
     * 保存用户权限到缓存中
     *
     * @param userPermissionCacheEntity userPermissionDo
     */
    void save(UserPermissionCacheEntity userPermissionCacheEntity);

    /**
     * 删除用户权限缓存
     *
     * @param uid uid
     */
    void remove(Long uid);

    /**
     * 清空所有用户权限缓存
     */
    void removeAll();

    /**
     * 清空所有用户权限缓存
     *
     * @param ids ids
     */
    void removeAll(Long... ids);

    /**
     * 获取用户权限key集合
     *
     * @param id id
     * @return List<String>
     */
    List<String> getPermissionKey(Long id);
}
