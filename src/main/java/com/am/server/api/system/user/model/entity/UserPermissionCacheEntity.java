package com.am.server.api.system.user.model.entity;

import com.am.server.api.system.permission.config.model.Menu;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author 阮雪峰
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Accessors(chain = true)
public class UserPermissionCacheEntity {
    private Long id;
    private List<Menu> permissionList;
    private List<String> permissionKeyList;
}
