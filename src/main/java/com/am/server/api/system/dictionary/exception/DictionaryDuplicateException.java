package com.am.server.api.system.dictionary.exception;

import com.am.server.common.base.exception.BusinessException;
import org.springframework.http.HttpStatus;

/**
 * @author 阮雪峰
 */
public class DictionaryDuplicateException extends BusinessException {
    public DictionaryDuplicateException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
