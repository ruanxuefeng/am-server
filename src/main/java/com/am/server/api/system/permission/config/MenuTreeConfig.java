package com.am.server.api.system.permission.config;

import com.am.server.api.system.permission.config.model.Menu;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 阮雪峰
 */
@Slf4j
@Setter
@Getter
@Component
@Configuration
@ConfigurationProperties(prefix = "menu")
@EnableConfigurationProperties(MenuTreeConfig.class)
public class MenuTreeConfig {
    private List<Menu> list;
}
