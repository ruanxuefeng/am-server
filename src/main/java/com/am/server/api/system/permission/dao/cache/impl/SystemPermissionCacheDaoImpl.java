package com.am.server.api.system.permission.dao.cache.impl;

import com.am.server.api.system.permission.dao.cache.SystemPermissionCacheDao;
import com.am.server.api.system.permission.model.entity.SystemPermissionCacheEntity;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Repository
public class SystemPermissionCacheDaoImpl implements SystemPermissionCacheDao {
    /**
     * 菜单缓存key模板
     */
    private static final String KEY = "system: permission";

    private final RedisTemplate<String, SystemPermissionCacheEntity> systemPermissionCacheRedisTemplate;

    public SystemPermissionCacheDaoImpl(RedisTemplate<String, SystemPermissionCacheEntity> systemPermissionCacheRedisTemplate) {
        this.systemPermissionCacheRedisTemplate = systemPermissionCacheRedisTemplate;
    }

    @Override
    public void save(SystemPermissionCacheEntity systemPermissionCacheEntity) {
        // 这个缓存不需要过期
        systemPermissionCacheRedisTemplate.opsForValue().set(KEY, systemPermissionCacheEntity, 0);
    }

    @Override
    public Optional<SystemPermissionCacheEntity> get() {
        return Optional.ofNullable(systemPermissionCacheRedisTemplate.opsForValue().get(KEY));
    }
}
