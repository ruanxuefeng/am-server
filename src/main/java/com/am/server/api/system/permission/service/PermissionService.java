package com.am.server.api.system.permission.service;

import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.api.system.permission.model.dto.MenuTreeDto;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface PermissionService {
    /**
     * 查询权限树
     *
     * @return List<MenuListVo>
     */
    List<MenuTreeDto> tree();

    /**
     * 根据id获取菜单
     *
     * @param menuIdList menuIdList
     * @return List<Menu>
     */
    List<Menu> findByIds(Collection<String> menuIdList);

    /**
     * 查询菜单
     *
     * @param menuId menuId
     * @return Menu
     */
    Optional<Menu> findById(String menuId);

    /**
     * 查询菜单树
     *
     * @return List<MenuTreeDto>
     */
    List<MenuTreeDto> menuTree();
}
