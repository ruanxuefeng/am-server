package com.am.server.api.system.user.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.am.server.api.system.user.model.dto.UpdateRoleDto;
import com.am.server.api.system.user.service.AdminUserService;
import com.am.server.common.util.AspectUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Service;

/**
 * 更新用户缓存
 *
 * @author 阮雪峰
 */
@Slf4j
@Service
@Aspect
public record UpdateUserPermissionCacheAction(AdminUserService adminUserService) {

    @After("execution(* com.am.server.api.system.user.service.AdminUserService.updateRole(..))")
    public void doAction(JoinPoint joinPoint) {
        final UpdateRoleDto updateRole = AspectUtils.getParameterValueByIndex(joinPoint, 0);
        if (CollUtil.isNotEmpty(updateRole.getIds())) {
            adminUserService.refreshPermissionAsync(updateRole.getIds());
        }
    }
}
