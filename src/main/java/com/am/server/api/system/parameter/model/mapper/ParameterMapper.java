package com.am.server.api.system.parameter.model.mapper;

import com.am.server.api.system.parameter.model.entity.ParameterCacheEntity;
import com.am.server.api.system.parameter.model.entity.ParameterEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

/**
 * @author 阮雪峰
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ParameterMapper {
    ParameterCacheEntity toCacheEntity(ParameterEntity entity);
}
