package com.am.server.api.system.user.model.dto;

import lombok.Data;

/**
 * 邮箱是否被使用
 *
 * @author 阮雪峰
 */
@Data
public class EmailExistQueryDto {
    /**
     * 用户id(在修改的时候必填)
     */
    private Long id;
    /**
     * 用户邮箱
     *
     * @required
     */
    private String email;
}
