package com.am.server.api.system.user.service;

import com.am.server.api.system.user.model.dto.*;
import com.am.server.common.base.model.dto.PageDto;

import java.util.List;

/**
 * @author 阮雪峰
 */
public interface AdminUserService {
    /**
     * 分页
     *
     * @param query query
     * @return PageVO<AdminUserListVO>
     * @author 阮雪峰
     */
    PageDto<AdminUserDto> find(AdminUserListQueryDto query);

    /**
     * 新增
     *
     * @param dto 用户信息
     * @author 阮雪峰
     */
    void save(SaveAdminUserDto dto);

    /**
     * 删除
     *
     * @param id 用户id
     * @author 阮雪峰
     */
    void delete(Long id);

    /**
     * 邮箱是否已被使用
     *
     * @param email email
     * @return boolean
     * @author 阮雪峰
     */
    Boolean isEmailExist(String email);

    /**
     * 管理员重置用户密码
     *
     * @param id 用户主键
     * @author 阮雪峰
     */
    void resetPassword(Long id);

    /**
     * 修改角色
     *
     * @param dto dto
     */
    void updateRole(UpdateRoleDto dto);

    /**
     * 管理员更新用户基本信息
     *
     * @param dto 用户信息
     * @author 阮雪峰
     */
    void update(UpdateAdminUserDto dto);

    /**
     * 查询角色id list
     *
     * @param id id
     * @return java.util.List<java.lang.Long>
     * @author 阮雪峰
     */
    List<Long> findRoleIdList(Long id);

    /**
     * 在所有的用户中，判断用户名是否存在
     *
     * @param username username
     * @return Boolean
     */
    Boolean isUsernameExist(String username);

    /**
     * 排除当前用户后，查询邮箱是否存在
     *
     * @param email email
     * @param id    id
     * @return Boolean
     */
    Boolean isEmailExistWithId(String email, Long id);

    /**
     * 排除当前用户后，查询用户名是否存在
     *
     * @param username username
     * @param id       id
     * @return Boolean
     */
    Boolean isUsernameExistWithId(String username, Long id);

    /**
     * 异步刷新已经登录了的用户权限
     *
     * @param id id
     */
    void refreshPermissionAsync(Long id);

    /**
     * 异步刷新已经登录了的用户权限
     *
     * @param ids ids
     */
    void refreshPermissionAsync(List<Long> ids);
}
