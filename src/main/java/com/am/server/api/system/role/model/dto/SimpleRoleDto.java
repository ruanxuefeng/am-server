package com.am.server.api.system.role.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 下拉选择的数据
 *
 * @author 阮雪峰
 */
@Setter
@Getter
public class SimpleRoleDto {
    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    private String name;
    /**
     * 主页名称
     */
    private String homePageId;
}
