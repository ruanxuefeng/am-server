package com.am.server.api.system.user.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.text.CharSequenceUtil;
import com.am.server.api.system.log.aspect.annotation.WriteLog;
import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.api.system.permission.service.UserPermissionCacheService;
import com.am.server.api.system.user.constant.UserLogConstant;
import com.am.server.api.system.user.constant.UserMessageConstant;
import com.am.server.api.system.user.model.dto.LoginDto;
import com.am.server.api.system.user.model.dto.LoginUserInfoDto;
import com.am.server.api.system.user.model.dto.UpdateLoginUserInfoDto;
import com.am.server.api.system.user.model.dto.UserInfoDto;
import com.am.server.api.system.user.service.LoginUserService;
import com.am.server.common.base.model.dto.ResponseMessageDto;
import com.am.server.common.base.validator.Login;
import com.am.server.common.constant.Constant;
import com.am.server.common.constant.MessageConstant;
import com.am.server.common.util.ResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 登录相关接口
 *
 * @author 阮雪峰
 */
@RestController
@RequestMapping(Constant.ADMIN_ROOT)
public class LoginController {
    private final LoginUserService loginUserService;
    private final UserPermissionCacheService userPermissionCacheService;

    public LoginController(final LoginUserService loginUserService, final UserPermissionCacheService userPermissionCacheService) {
        this.loginUserService = loginUserService;
        this.userPermissionCacheService = userPermissionCacheService;
    }

    /**
     * 登录
     *
     * @param query 用户
     * @return org.springframework.http.CustomResponseEntity
     */
    @WriteLog(UserLogConstant.USER_LOGIN)
    @PostMapping("/login")
    public ResponseEntity<LoginUserInfoDto> login(@Validated(Login.class) @RequestBody final LoginDto query) {
        return ResponseUtils.ok(loginUserService.login(query));
    }

    /**
     * 获取用户信息
     *
     * @return ResponseEntity
     */
    @SaCheckLogin
    @GetMapping("/user/info")
    public ResponseEntity<UserInfoDto> info() {
        return ResponseUtils.ok(loginUserService.info(StpUtil.getLoginIdAsLong()));
    }

    /**
     * 修改登录用户个人信息
     *
     * @param user 用户信息
     * @return org.springframework.http.ResponseEntity
     */
    @SaCheckLogin
    @PostMapping("/user/update/info")
    public ResponseEntity<ResponseMessageDto> updateUserInfo(@Validated final UpdateLoginUserInfoDto user) {
        user.setId(StpUtil.getLoginIdAsLong());
        if (CharSequenceUtil.isEmpty(user.getAvatar()) && user.getImg() == null) {
            return ResponseUtils.badRequest(UserMessageConstant.NO_AVATAR);
        }
        loginUserService.update(user);
        return ResponseUtils.ok(MessageConstant.UPDATE_SUCCESS);
    }

    /**
     * 获取用户菜单
     *
     * @return ResponseEntity
     */
    @SaCheckLogin
    @GetMapping("/user/menuList")
    public ResponseEntity<List<Menu>> menuList() {
        return ResponseUtils.ok(loginUserService.getMenuList());
    }

    /**
     * 获取权限key
     *
     * @return ResponseEntity
     */
    @SaCheckLogin
    @GetMapping("/user/permissionKeyList")
    public ResponseEntity<List<String>> permissionKeyList() {
        return ResponseUtils.ok(userPermissionCacheService.getPermissionList(StpUtil.getLoginIdAsLong(), null));
    }

    /**
     * 退出登录
     *
     * @return org.springframework.http.ResponseEntity
     */
    @SaCheckLogin
    @GetMapping("/logout")
    public ResponseEntity<ResponseMessageDto> logout() {
        loginUserService.logout();
        return ResponseUtils.ok(MessageConstant.SUCCESS);
    }
}
