package com.am.server.api.system.parameter.service;

/**
 * @author 阮雪峰
 */
public interface SynchronizeParameterService {
    /**
     * 同步系统参数
     */
    void load();
}
