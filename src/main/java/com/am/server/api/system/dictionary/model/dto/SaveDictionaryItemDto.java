package com.am.server.api.system.dictionary.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class SaveDictionaryItemDto {
    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    private String name;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 备注
     */
    private String memo;
    /**
     * 对应字典id
     */
    private Long dictionaryId;
}
