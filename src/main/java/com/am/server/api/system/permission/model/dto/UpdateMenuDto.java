package com.am.server.api.system.permission.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 阮雪锋
 */
@Setter
@Getter
public class UpdateMenuDto {
    /**
     * 主键
     */
    @NotNull(message = "menu.title.id")
    private Long id;

    /**
     * 标题
     */
    @NotBlank(message = "menu.title.blank")
    private String title;

    /**
     * 关键字
     */
    @NotBlank(message = "menu.keyword.blank")
    private String keyword;

    /**
     * 排序
     */
    @NotNull(message = "menu.sort.null")
    private Integer sort;

    /**
     * 描述
     */
    private String memo;

    /**
     * 父级主键
     */
    private Long parentId;
}
