package com.am.server.api.system.parameter.config;

import com.am.server.api.system.parameter.model.entity.ParameterCacheEntity;
import com.am.server.config.cache.RedisConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;

/**
 * @author 阮雪峰
 */
@Component
public class ParameterCacheConfig {
    /**
     * 菜单缓存
     *
     * @param redisConnectionFactory redisConnectionFactory
     * @return RedisTemplate<String, Menu>
     */
    @Bean("parameterCacheCacheRedisTemplate")
    public RedisTemplate<String, ParameterCacheEntity> parameterCacheCacheRedisTemplate(RedisConnectionFactory redisConnectionFactory) {

        RedisTemplate<String, ParameterCacheEntity> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
        Jackson2JsonRedisSerializer<ParameterCacheEntity> serializer = new Jackson2JsonRedisSerializer<>(ParameterCacheEntity.class);

        return RedisConfig.configTemplate(template, serializer);
    }
}
