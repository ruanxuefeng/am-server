package com.am.server.api.system.user.constant;

/**
 * @author 阮雪峰
 */
public class UserMessageConstant {
    private UserMessageConstant() {
    }

    /**
     * 邮箱已存在
     */
    public static final String EMAIL_EXIST = "邮箱已存在";
    /**
     * 用户名已存在
     */
    public static final String USERNAME_EXIST = "用户名已存在";
    /**
     * 不允许删除自己
     */
    public static final String NOT_ALLOW_DELETE_YOURSELF = "不允许删除自己";
    /**
     * 密码重置成功，下次登录时生效
     */
    public static final String PASSWORD_RESET_SUCCESS = "密码重置成功，下次登录时生效";

    /**
     * 请上传头像
     */
    public static final String NO_AVATAR = "请上传头像";
}
