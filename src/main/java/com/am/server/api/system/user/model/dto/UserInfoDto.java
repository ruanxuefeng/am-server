package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.model.enumerate.Gender;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 用户信息
 *
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Data
public class UserInfoDto {

    /**
     * 主键
     */
    private Long id;

    /**
     * 登录名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别
     */
    private Gender gender;

    /**
     * 主页
     */
    private String homePath;

    /**
     * 角色
     */
    private List<String> roles;
}
