package com.am.server.api.system.dictionary.model.entity;

import com.am.server.common.base.model.entity.EditableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @author 阮雪峰
 */
@Table(name = "dictionary")
@Entity
@Setter
@Getter
public class DictionaryEntity extends EditableEntity {
    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 备注
     */
    private String memo;

    @OneToMany(mappedBy = "dictionary", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<DictionaryItemEntity> items;
}
