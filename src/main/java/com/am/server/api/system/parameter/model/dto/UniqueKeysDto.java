package com.am.server.api.system.parameter.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class UniqueKeysDto {
    /**
     * 唯一值
     */
    private List<String> uniqueKeys;
}
