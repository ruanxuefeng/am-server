package com.am.server.api.system.user.exception;

import com.am.server.common.base.exception.BusinessException;
import com.am.server.common.constant.MessageConstant;
import org.springframework.http.HttpStatus;

/**
 * 没有权限访问异常
 *
 * @author 阮雪峰
 */
public class NoPermissionAccessException extends BusinessException {
    public NoPermissionAccessException() {
        super(MessageConstant.NO_PERMISSION, HttpStatus.UNAUTHORIZED);
    }
}
