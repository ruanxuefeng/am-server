package com.am.server.api.system.role.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.am.server.api.system.log.aspect.annotation.WriteLog;
import com.am.server.api.system.role.constant.RoleLogConstant;
import com.am.server.api.system.role.constant.RolePermissionConstant;
import com.am.server.api.system.role.model.dto.*;
import com.am.server.api.system.role.service.RoleService;
import com.am.server.common.base.model.dto.PageDto;
import com.am.server.common.base.model.dto.ResponseMessageDto;
import com.am.server.common.constant.Constant;
import com.am.server.common.constant.MessageConstant;
import com.am.server.common.util.ResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 角色管理
 *
 * @author 阮雪峰
 */
@SaCheckLogin
@WriteLog(RoleLogConstant.MENU_NAME)
@RestController
@RequestMapping(Constant.ADMIN_ROOT + "/role")
public class RoleController {

    private final RoleService roleService;


    public RoleController(final RoleService roleService) {
        this.roleService = roleService;
    }

    /**
     * 列表查询
     *
     * @param query roleListAo
     * @return org.springframework.http.ResponseEntity
     */
    @GetMapping("/find")
    public ResponseEntity<PageDto<RoleDto>> find(RoleListQueryDto query) {
        return ResponseUtils.ok(roleService.find(query));
    }

    /**
     * 新增
     *
     * @param role 角色信息
     * @return org.springframework.http.ResponseEntity
     */
    @SaCheckPermission(RolePermissionConstant.ROLE_PERMISSION_ADD)
    @WriteLog("新增")
    @PostMapping("/save")
    public ResponseEntity<ResponseMessageDto> save(@Validated @RequestBody SaveRoleDto role) {
        roleService.save(role);
        return ResponseUtils.ok(MessageConstant.SAVE_SUCCESS);
    }

    /**
     * 修改
     *
     * @param roleAo 角色信息
     * @return org.springframework.http.ResponseEntity
     * @author 阮雪峰
     */
    @SaCheckPermission(RolePermissionConstant.ROLE_PERMISSION_UPDATE)
    @WriteLog("修改")
    @PostMapping("/update")
    public ResponseEntity<ResponseMessageDto> update(@Validated @RequestBody UpdateRoleDto roleAo) {
        roleService.update(roleAo);
        return ResponseUtils.ok(MessageConstant.UPDATE_SUCCESS);
    }

    /**
     * 删除
     *
     * @param id 角色信息
     * @return org.springframework.http.ResponseEntity
     */
    @SaCheckPermission(RolePermissionConstant.ROLE_PERMISSION_DELETE)
    @WriteLog("删除")
    @DeleteMapping("/delete")
    public ResponseEntity<ResponseMessageDto> delete(Long id) {
        return Optional.ofNullable(id)
                .map(i -> {
                    roleService.delete(id);
                    return ResponseUtils.ok(MessageConstant.DELETE_SUCCESS);
                })
                .orElse(ResponseUtils.ok(MessageConstant.COMMON_DELETE_PRIMARY_KEY_NULL));
    }

    /**
     * 角色拥有的权限
     *
     * @param id role
     * @return org.springframework.http.ResponseEntity
     */
    @SaCheckPermission(RolePermissionConstant.ROLE_PERMISSION_AUTHORIZE)
    @GetMapping("/permissions")
    public ResponseEntity<List<String>> permissions(Long id) {
        return ResponseUtils.ok(roleService.findPermissions(id));
    }

    /**
     * 修改权限
     *
     * @param updateRolePermissionDto 角色信息
     * @return org.springframework.http.ResponseEntity
     */
    @SaCheckPermission(RolePermissionConstant.ROLE_PERMISSION_AUTHORIZE)
    @PostMapping("/authorize")
    public ResponseEntity<ResponseMessageDto> authorize(@RequestBody UpdateRolePermissionDto updateRolePermissionDto) {
        roleService.updatePermissions(updateRolePermissionDto);
        return ResponseUtils.ok(MessageConstant.UPDATE_SUCCESS);
    }

    /**
     * 查询所有
     *
     * @return org.springframework.http.ResponseEntity
     */
    @GetMapping("/all")
    public ResponseEntity<List<SimpleRoleDto>> all() {
        return ResponseUtils.ok(roleService.findAll());
    }
}
