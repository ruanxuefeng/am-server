package com.am.server.api.system.permission.exception;

import com.am.server.common.base.exception.BusinessException;
import org.springframework.http.HttpStatus;

/**
 * 菜单不存在
 *
 * @author 阮雪峰
 */
public class MenuNotExistException extends BusinessException {
    public MenuNotExistException() {
        super("菜单不存在", HttpStatus.BAD_REQUEST);
    }
}
