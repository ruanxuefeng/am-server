package com.am.server.api.system.log.model.entity;

import com.am.server.common.base.model.entity.BaseEntity;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author 阮雪峰
 */
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Entity
@Table(name = "log")
@Getter
@Setter
@ToString(of = {"name", "menu", "operate"})
public class LogEntity extends BaseEntity {

    /**
     * 操作人
     */
    @Column(name = "name")
    private String name;

    /**
     * 菜单
     */
    @Column(name = "menu")
    private String menu;

    /**
     * 操作
     */
    @Column(name = "operate")
    private String operate;

    /**
     * ip地址
     */
    @Column(name = "ip")
    private String ip;

    @Column(updatable = false)
    private LocalDateTime createdTime;

    @Type(type = "json")
    @Column(name = "extend", columnDefinition = "json")
    private LogExtendEntity extend;
}


