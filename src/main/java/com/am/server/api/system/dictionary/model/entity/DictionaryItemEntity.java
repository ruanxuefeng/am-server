package com.am.server.api.system.dictionary.model.entity;

import com.am.server.common.base.model.entity.EditableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author 阮雪峰
 */
@Table(name = "dictionary_item")
@Entity
@Setter
@Getter
public class DictionaryItemEntity extends EditableEntity {
    @Column(name = "dictionary_id")
    private Long dictionaryId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "dictionary_id", insertable = false, updatable = false)
    private DictionaryEntity dictionary;
    /**
     * 名称
     */
    private String name;
    /**
     * 备注
     */
    private String memo;
    /**
     * 排序
     */
    private Integer sort;
}
