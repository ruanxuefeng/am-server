package com.am.server.api.system.log.dao.rbd.impl;

import cn.hutool.core.util.StrUtil;
import com.am.server.api.system.log.dao.rbd.LogDao;
import com.am.server.api.system.log.model.dto.LogListQueryDto;
import com.am.server.api.system.log.model.entity.LogEntity;
import com.am.server.api.system.log.repository.LogRepository;
import com.am.server.common.constant.QueryConstant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Service
public class LogDaoImpl implements LogDao {

    private final LogRepository logRepository;

    public LogDaoImpl(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    @Override
    public void save(LogEntity log) {
        logRepository.save(log);
    }

    @Override
    public Page<LogEntity> findAll(LogListQueryDto logListQueryDto) {
        return logRepository.findAll(
                (root, query, cb) -> {
                    Predicate predicate = cb.conjunction();
                    Optional.ofNullable(logListQueryDto.getName())
                            .filter(StrUtil::isNotBlank)
                            .ifPresent(name -> predicate.getExpressions().add(cb.like(root.get("name"), String.format(QueryConstant.FULL_LIKE_QUERY_FORMAT, logListQueryDto.getName()))));

                    Optional.ofNullable(logListQueryDto.getOperate())
                            .filter(StrUtil::isNotBlank)
                            .ifPresent(operate -> predicate.getExpressions().add(cb.like(root.get("operate"), String.format(QueryConstant.FULL_LIKE_QUERY_FORMAT, logListQueryDto.getOperate()))));

                    Optional.ofNullable(logListQueryDto.getMenu())
                            .filter(StrUtil::isNotBlank)
                            .ifPresent(menu -> predicate.getExpressions().add(cb.like(root.get("menu"), String.format(QueryConstant.FULL_LIKE_QUERY_FORMAT, logListQueryDto.getMenu()))));

                    if (logListQueryDto.getStartDate() != null && logListQueryDto.getEndDate() != null) {
                        LocalDateTime startDateTime = LocalDateTime.of(logListQueryDto.getStartDate(), LocalTime.MIN);
                        LocalDateTime endDateTime = LocalDateTime.of(logListQueryDto.getStartDate(), LocalTime.MAX);
                        predicate.getExpressions().add(cb.between(root.get("createdTime"), startDateTime, endDateTime));
                    }
                    query.orderBy(cb.desc(root.get("id")));
                    return predicate;
                },
                PageRequest.of(logListQueryDto.getPage() - 1, logListQueryDto.getPageSize()));
    }
}
