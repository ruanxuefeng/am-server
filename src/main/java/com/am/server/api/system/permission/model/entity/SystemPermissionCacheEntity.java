package com.am.server.api.system.permission.model.entity;

import com.am.server.api.system.permission.config.model.Menu;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统权限缓存实体
 *
 * @author 阮雪峰
 */
@AllArgsConstructor
@Setter
@Getter
public class SystemPermissionCacheEntity {
    /**
     * 全部权限的树形结构数据
     */
    private List<Menu> allTree;
    /**
     * 菜单树结构，不包括按钮权限
     */
    private List<Menu> menuTree;
    /**
     * 所有菜单平铺数据，包括菜单和按钮
     */
    private List<Menu> menuList;
    /**
     * 所有菜单的key值
     */
    private List<String> keyList;

    public SystemPermissionCacheEntity() {
        menuTree = new ArrayList<>();
        menuList = new ArrayList<>();
        keyList = new ArrayList<>();
    }
}
