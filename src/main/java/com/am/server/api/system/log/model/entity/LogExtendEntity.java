package com.am.server.api.system.log.model.entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * @author 阮雪峰
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, property = "type")
public interface LogExtendEntity extends Serializable {
}
