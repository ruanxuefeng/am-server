package com.am.server.api.system.role.service;

import com.am.server.api.system.role.model.dto.*;
import com.am.server.common.base.model.dto.PageDto;

import java.util.List;

/**
 * 角色
 *
 * @author 阮雪峰
 */
public interface RoleService {

    /**
     * 列表
     *
     * @param query query
     * @return PageVO<RoleListVo>
     * @author 阮雪峰
     */
    PageDto<RoleDto> find(RoleListQueryDto query);

    /**
     * 新增
     *
     * @param dto dto
     * @author 阮雪峰
     */
    void save(SaveRoleDto dto);

    /**
     * 修改
     *
     * @param dto dto
     * @author 阮雪峰
     */
    void update(UpdateRoleDto dto);

    /**
     * 删除
     *
     * @param id id
     * @author 阮雪峰
     */
    void delete(Long id);

    /**
     * 查所角色
     *
     * @return List<SimpleRoleVo>
     * @author 阮雪峰
     */
    List<SimpleRoleDto> findAll();


    /**
     * 更新权限
     *
     * @param dto dto
     */
    void updatePermissions(UpdateRolePermissionDto dto);

    /**
     * 角色拥有的权限
     *
     * @param id 主键
     * @return List<String>
     */
    List<String> findPermissions(Long id);
}
