package com.am.server.api.system.dictionary.service;

import com.am.server.api.system.dictionary.model.dto.DictionaryDto;
import com.am.server.api.system.dictionary.model.dto.DictionaryListQueryDto;
import com.am.server.api.system.dictionary.model.dto.SaveDictionaryDto;
import com.am.server.api.system.dictionary.model.dto.UpdateDictionaryDto;
import com.am.server.common.base.model.dto.PageDto;

/**
 * @author 阮雪峰
 */
public interface DictionaryService {

    /**
     * 分页查询
     *
     * @param dto dto
     * @return PageDto<DictionaryDto>
     */
    PageDto<DictionaryDto> find(DictionaryListQueryDto dto);

    /**
     * 新增
     *
     * @param dto dto
     */
    void save(SaveDictionaryDto dto);

    /**
     * 修改
     *
     * @param dto dto
     */
    void update(UpdateDictionaryDto dto);

    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(Long id);

    /**
     * 字典编码是否存在
     *
     * @param code code
     * @return Boolean
     */
    Boolean exist(String code);

    /**
     * 排除当前数据，判断编码是否存在
     *
     * @param code code
     * @param id   需要修改的数据id
     * @return Boolean
     */
    Boolean exist(String code, Long id);
}
