package com.am.server.api.system.dictionary.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.am.server.api.system.dictionary.constant.DictionaryLogConstant;
import com.am.server.api.system.dictionary.exception.DictionaryDuplicateException;
import com.am.server.api.system.dictionary.model.dto.DictionaryDto;
import com.am.server.api.system.dictionary.model.dto.DictionaryListQueryDto;
import com.am.server.api.system.dictionary.model.dto.SaveDictionaryDto;
import com.am.server.api.system.dictionary.model.dto.UpdateDictionaryDto;
import com.am.server.api.system.dictionary.service.DictionaryService;
import com.am.server.api.system.log.aspect.annotation.WriteLog;
import com.am.server.common.base.model.dto.PageDto;
import com.am.server.common.base.model.dto.ResponseMessageDto;
import com.am.server.common.constant.Constant;
import com.am.server.common.constant.MessageConstant;
import com.am.server.common.util.ResponseUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 字典管理
 *
 * @author 阮雪峰
 */
@SaCheckLogin
@WriteLog(DictionaryLogConstant.MENU_NAME)
@RestController
@RequestMapping(Constant.ADMIN_ROOT + "/dictionary")
public class DictionaryController {
    private final DictionaryService dictionaryService;

    public DictionaryController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    /**
     * 列表查询
     *
     * @param dto dto
     * @return org.springframework.http.ResponseEntity
     */
    @GetMapping("/find")
    public ResponseEntity<PageDto<DictionaryDto>> find(DictionaryListQueryDto dto) {
        return ResponseUtils.ok(dictionaryService.find(dto));
    }

    /**
     * 新增
     *
     * @param dto dto
     * @return ResponseEntity
     */
    @WriteLog("新增")
    @PostMapping("/save")
    public ResponseEntity<ResponseMessageDto> save(@Validated @RequestBody SaveDictionaryDto dto) {
        if (Boolean.TRUE.equals(dictionaryService.exist(dto.getCode()))) {
            throw new DictionaryDuplicateException(String.format("字典编码<%s>已存在", dto.getCode()));
        }
        dictionaryService.save(dto);
        return ResponseUtils.ok(MessageConstant.SAVE_SUCCESS);
    }

    /**
     * 修改
     *
     * @param dto dto
     * @return ResponseEntity
     */
    @WriteLog("修改")
    @PostMapping("/update")
    public ResponseEntity<ResponseMessageDto> update(@Validated @RequestBody UpdateDictionaryDto dto) {
        if (Boolean.TRUE.equals(dictionaryService.exist(dto.getCode(), dto.getId()))) {
            throw new DictionaryDuplicateException(String.format("字典编码<%s>已存在", dto.getCode()));
        }
        dictionaryService.update(dto);
        return ResponseUtils.ok(MessageConstant.UPDATE_SUCCESS);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return ResponseEntity
     */
    @WriteLog("删除")
    @DeleteMapping("/delete")
    public ResponseEntity<ResponseMessageDto> delete(Long id) {
        return Optional.ofNullable(id)
                .map(i -> {
                    dictionaryService.delete(id);
                    return ResponseUtils.ok(MessageConstant.DELETE_SUCCESS);
                })
                .orElse(ResponseUtils.ok(MessageConstant.COMMON_DELETE_PRIMARY_KEY_NULL));
    }
}
