package com.am.server.api.system.upload.service.impl;

import com.am.server.api.system.upload.config.LocalFileUploadConfig;
import com.am.server.api.system.upload.exception.UploadFileException;
import com.am.server.api.system.upload.service.FileUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * 本地文件上传
 *
 * @author 阮雪峰
 */
@Slf4j
public class LocalFileUploadServiceImpl implements FileUploadService {

    @Resource
    private LocalFileUploadConfig config;

    @Override
    public String upload(MultipartFile file, String key) {

        try {
            File file1 = new File(config.getFilePath() + key);
            if (!file1.getParentFile().exists() && file1.getParentFile().mkdirs()) {
                log.info("创建文件夹成功（{}）", config.getFilePath() + key);
            }
            file.transferTo(file1);
            return config.getRequestUrl() + config.getUri() + key + "?t=" + System.currentTimeMillis();
        } catch (IOException e) {
            throw new UploadFileException(e);
        }
    }

    @Override
    public void remove(String key) {
        File f = new File(key);
        try {
            Files.delete(f.toPath());
        } catch (IOException e) {
            log.error("删除文件失败，路径：{}", f.getPath());
        }
    }
}
