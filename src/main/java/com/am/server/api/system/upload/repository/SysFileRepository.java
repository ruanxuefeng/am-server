package com.am.server.api.system.upload.repository;

import com.am.server.api.system.upload.model.entity.SysFileEntity;
import com.am.server.common.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 阮雪峰
 */
@Repository
public interface SysFileRepository extends BaseRepository<SysFileEntity> {
}
