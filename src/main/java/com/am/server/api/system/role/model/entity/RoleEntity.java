package com.am.server.api.system.role.model.entity;


import com.am.server.api.system.user.model.entity.AdminUserEntity;
import com.am.server.common.base.model.entity.EditableEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

/**
 * @author 阮雪峰
 */
@Entity
@Table(name = "role")
@Accessors(chain = true)
@Getter
@Setter
public class RoleEntity extends EditableEntity {
    private String name;

    @Column(name = "home_page_id")
    private String homePageId;

    private String memo;

    @ElementCollection
    @CollectionTable(name = "role_permission", joinColumns = @JoinColumn(name = "role"))
    @Column(name = "permission")
    private List<String> permissions;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @OrderBy("id desc")
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "role")},
            inverseJoinColumns = {@JoinColumn(name = "user")})
    private List<AdminUserEntity> users;

    @Override
    public String toString() {
        return "RoleDO{" +
                "id=" + getId() +
                "name='" + name + '\'' +
                ", memo='" + memo + '\'' +
                '}';
    }
}
