package com.am.server.api.system.upload.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author 阮雪峰
 */
@AllArgsConstructor
@Setter
@Getter
public class SysFileUrlDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 1760779639702537957L;
    private String url;

}
