package com.am.server.api.system.role.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 修改角色权限
 *
 * @author 阮雪峰
 */
@Data
public class UpdateRoleMenuDto {
    /**
     * id
     */
    @NotNull(message = "common.operate.primaryKey.null")
    private Long id;

    /**
     * 权限id数组
     */
    private List<Long> menuList;
}
