package com.am.server.api.system.parameter.model.dto;

import com.am.server.api.system.parameter.model.enumerate.ParameterType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author 阮雪峰
 */
@ToString(exclude = {"name"})
@Setter
@Getter
public class ParameterTreeDto {
    /**
     * 主键
     */
    private Long id;
    /**
     * 参数名
     */
    private String name;
    /**
     * 唯一key值
     */
    private String uniqueKey;
    /**
     * 参数值
     */
    private String value;
    /**
     * 是否事叶子节点
     */
    private Boolean leaf;
    /**
     * 类型
     */
    private ParameterType type;
    /**
     * 是否为系统参数，系统参数不允许删除和修改唯一key
     */
    private Boolean system;
    /**
     * 在代码中已经删除
     */
    private Boolean delete;
    /**
     * 子集
     */
    private List<ParameterTreeDto> children;
}
