package com.am.server.api.system.upload.model.entity;


import com.am.server.api.system.upload.enumerate.FileType;
import com.am.server.common.base.model.entity.EditableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 系统所用到的文件
 *
 * @author 阮雪峰
 */
@Table(name = "sys_file")
@Entity
@Setter
@Getter
public class SysFileEntity extends EditableEntity {
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 类型
     */
    private FileType type;
    /**
     * 网络请求路径
     */
    private String url;
    /**
     * 本地资源访问路径,相对路径
     */
    private String dir;
}
