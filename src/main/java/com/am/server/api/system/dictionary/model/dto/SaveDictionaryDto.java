package com.am.server.api.system.dictionary.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class SaveDictionaryDto {
    /**
     * 编码
     */
    @NotBlank(message = "编码不能为空")
    private String code;
    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    private String name;
    /**
     * 备注
     */
    private String memo;
}
