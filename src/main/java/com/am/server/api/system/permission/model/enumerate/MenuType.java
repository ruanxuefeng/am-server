package com.am.server.api.system.permission.model.enumerate;

import lombok.Getter;

/**
 * 菜单类型
 *
 * @author 阮雪峰
 */
public enum MenuType {
    /**
     * 菜单
     */
    MENU("菜单"),
    /**
     * 链接
     */
    LINK("链接"),
    /**
     * 按钮
     */
    ACTION("按钮");

    @Getter
    private final String description;

    MenuType(String description) {
        this.description = description;
    }
}
