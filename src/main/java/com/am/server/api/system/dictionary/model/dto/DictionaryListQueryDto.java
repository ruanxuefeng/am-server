package com.am.server.api.system.dictionary.model.dto;

import com.am.server.common.base.model.dto.PageQueryDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class DictionaryListQueryDto extends PageQueryDto {
    private String name;
}
