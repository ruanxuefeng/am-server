package com.am.server.api.system.upload.dao.rdb.impl;

import com.am.server.api.system.upload.dao.rdb.SysFileDao;
import com.am.server.api.system.upload.model.entity.SysFileEntity;
import com.am.server.api.system.upload.repository.SysFileRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Service
public class SysFileDaoImpl implements SysFileDao {
    private final SysFileRepository sysFileRepository;

    public SysFileDaoImpl(final SysFileRepository sysFileRepository) {
        this.sysFileRepository = sysFileRepository;
    }

    @Override
    public void save(final SysFileEntity sysFile) {
        sysFileRepository.save(sysFile);
    }

    @Override
    public void delete(SysFileEntity entity) {
        sysFileRepository.delete(entity);
    }

    @Override
    public Optional<SysFileEntity> findById(Long id) {
        return sysFileRepository.findById(id);
    }

    @Override
    public void deleteAll(List<SysFileEntity> entities) {
        sysFileRepository.deleteAll(entities);
    }
}
