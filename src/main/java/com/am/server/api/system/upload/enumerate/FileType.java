package com.am.server.api.system.upload.enumerate;

/**
 * 文件类型
 *
 * @author 阮雪峰
 */
public enum FileType {
    /**
     * 头像
     */
    AVATAR("/avatar"),
    ;

    /**
     * 文件夹
     */
    private final String folder;

    FileType(String folder) {
        this.folder = folder;
    }

    public String getFolder() {
        return folder;
    }
}
