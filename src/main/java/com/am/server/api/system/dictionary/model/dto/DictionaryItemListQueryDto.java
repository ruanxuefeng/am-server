package com.am.server.api.system.dictionary.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class DictionaryItemListQueryDto {
    /**
     * 所属字典id
     */
    private Long dictionaryId;
    /**
     * 所属字典编码
     */
    private String dictionaryCode;
    /**
     * 名称
     */
    private String name;
}
