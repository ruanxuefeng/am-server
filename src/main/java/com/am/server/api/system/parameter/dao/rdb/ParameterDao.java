package com.am.server.api.system.parameter.dao.rdb;

import com.am.server.api.system.parameter.model.dto.ParameterListQueryDto;
import com.am.server.api.system.parameter.model.entity.ParameterEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface ParameterDao {
    /**
     * 查询所有
     *
     * @return List<ParameterEntity>
     */
    List<ParameterEntity> findAll();

    /**
     * 查询所有
     *
     * @return List<ParameterEntity>
     */
    List<ParameterEntity> findAll(ParameterListQueryDto query);

    /**
     * 保存所有
     *
     * @param collection collection
     */
    void saveAll(Collection<ParameterEntity> collection);

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKey uniqueKey
     * @return ParameterEntity
     */
    Optional<ParameterEntity> findByUniqueKey(String uniqueKey);

    /**
     * 保存
     *
     * @param parameter parameter
     */
    void save(ParameterEntity parameter);

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKeys uniqueKeys
     * @return List<ParameterVo>
     */
    List<ParameterEntity> findByUniqueKeys(List<String> uniqueKeys);
}
