package com.am.server.api.system.parameter.dao.cache.impl;

import cn.hutool.core.collection.CollUtil;
import com.am.server.api.system.parameter.dao.cache.ParameterCacheDao;
import com.am.server.api.system.parameter.model.entity.ParameterCacheEntity;
import com.am.server.api.system.parameter.model.entity.ParameterEntity;
import com.am.server.api.system.parameter.model.mapper.ParameterMapper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ParameterCacheDaoImpl implements ParameterCacheDao {
    private static final String KEY_TEMPLATE = "system: parameter: %s";

    private final RedisTemplate<String, ParameterCacheEntity> parameterCacheCacheRedisTemplate;
    private final ParameterMapper parameterMapper;

    public ParameterCacheDaoImpl(RedisTemplate<String, ParameterCacheEntity> parameterCacheCacheRedisTemplate, ParameterMapper parameterMapper) {
        this.parameterCacheCacheRedisTemplate = parameterCacheCacheRedisTemplate;
        this.parameterMapper = parameterMapper;
    }

    @Override
    public void save(ParameterEntity parameter) {
        Optional.ofNullable(parameter)
                .map(parameterMapper::toCacheEntity)
                .ifPresent(parameterCache -> parameterCacheCacheRedisTemplate.opsForValue().set(createKey(parameter.getId()), parameterCache));
    }

    @Override
    public void save(ParameterCacheEntity parameter) {
        Optional.ofNullable(parameter)
                .ifPresent(parameterCache -> parameterCacheCacheRedisTemplate.opsForValue().set(createKey(parameter.getId()), parameterCache));
    }

    @Override
    public void saveAll(List<ParameterEntity> parameters) {
        parameters.stream().map(parameterMapper::toCacheEntity).forEach(this::save);
    }

    @Override
    public ParameterCacheEntity get(Long id) {
        return parameterCacheCacheRedisTemplate.opsForValue().get(createKey(id));
    }

    @Override
    public List<ParameterCacheEntity> getAll(Collection<Long> ids) {
        return parameterCacheCacheRedisTemplate.opsForValue().multiGet(ids.stream().map(this::createKey).collect(Collectors.toSet()));
    }

    @Override
    public List<ParameterCacheEntity> getAll() {
        Set<String> keys = parameterCacheCacheRedisTemplate.keys(String.format(KEY_TEMPLATE, "*"));
        if (CollUtil.isEmpty(keys)) {
            return Collections.emptyList();
        }
        return keys.stream()
                .map(key -> Objects.requireNonNull(parameterCacheCacheRedisTemplate.opsForValue().get(key)))
                .toList();
    }

    @Override
    public void remove(Long id) {
        parameterCacheCacheRedisTemplate.delete(createKey(id));
    }

    @Override
    public void clear(Collection<Long> ids) {
        parameterCacheCacheRedisTemplate.delete(ids.stream().map(this::createKey).collect(Collectors.toSet()));
    }

    @Override
    public void clear() {
        Set<String> keys = parameterCacheCacheRedisTemplate.keys(String.format(KEY_TEMPLATE, "*"));
        if (!CollectionUtils.isEmpty(keys)) {
            parameterCacheCacheRedisTemplate.delete(keys);
        }
    }

    private String createKey(Long id) {
        return String.format(KEY_TEMPLATE, id);
    }
}
