package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.model.enumerate.Gender;
import com.am.server.common.constant.RegularConstant;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 修改用户信息
 *
 * @author 阮雪峰
 */
@Getter
@Setter
public class UpdateAdminUserDto {

    /**
     * 主键
     */
    @NotNull(message = "common.operate.primaryKey.null")
    private Long id;

    /**
     * 用户名
     */
    @NotBlank(message = "login.username.blank")
    @Length(max = 64, message = "user.username.long")
    private String username;

    /**
     * 邮箱
     */
    @NotBlank(message = "user.email.blank")
    @Email(regexp = RegularConstant.EMAIL, message = "user.email.format")
    private String email;

    /**
     * 性别
     */
    private Gender gender;
}
