package com.am.server.api.system.permission.model.dto;

import com.am.server.api.system.permission.model.enumerate.MenuType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author 阮雪锋
 */
@Setter
@Getter
public class SaveMenuDto {

    /**
     * 菜单类型
     */
    private MenuType type;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * path
     */
    private String path;
    /**
     * name
     */
    private String name;
    /**
     * 组件
     */
    private String component;
    /**
     * 排序
     */
    private Integer orderNo;

    @NotBlank(message = "menu.title.blank")
    private String title;
    /**
     * Whether to ignore permissions
     */
    private Boolean ignoreAuth;
    /**
     * Whether not to cache
     */
    private Boolean ignoreKeepAlive;
    /**
     * Is it fixed on tab
     */
    private Boolean affix;
    /**
     * icon on tab
     */
    private String icon;

    private String frameSrc;

    /**
     * current page transition
     */
    private String transitionName;

    /**
     * Whether the route has been dynamically added
     */
    private Boolean hideBreadcrumb;

    /**
     * Hide submenu
     */
    private Boolean hideChildrenInMenu;

    /**
     * Carrying parameters
     */
    private Boolean carryParam;

    /**
     * Used internally to mark single-level menus
     */
    private Boolean single;

    /**
     * Currently active menu
     */
    private String currentActiveMenu;

    /**
     * Never show in tab
     */
    private Boolean hideTab;

    /**
     * Never show in menu
     */
    private Boolean hideMenu;

    private Boolean isLink;
}
