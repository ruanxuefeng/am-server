package com.am.server.api.system.user.dao.cache.impl;

import com.am.server.api.system.user.dao.cache.UserPermissionCacheDao;
import com.am.server.api.system.user.model.entity.UserPermissionCacheEntity;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author 阮雪峰
 */
@Repository
public class UserPermissionCacheDaoImpl implements UserPermissionCacheDao {
    private static final String KEY_TEMPLATE = "user: permission: %s";
    /**
     * 权限缓存过期时间为2小时
     */
    private static final int PERMISSION_CACHE_HOUR = 2;

    private final RedisTemplate<String, UserPermissionCacheEntity> userPermissionCacheRedisTemplate;

    public UserPermissionCacheDaoImpl(RedisTemplate<String, UserPermissionCacheEntity> userPermissionCacheRedisTemplate) {
        this.userPermissionCacheRedisTemplate = userPermissionCacheRedisTemplate;
    }

    @Override
    public UserPermissionCacheEntity findById(Long uid) {
        return userPermissionCacheRedisTemplate.opsForValue().get(String.format(KEY_TEMPLATE, uid));
    }

    @Override
    public void save(UserPermissionCacheEntity userPermissionCacheEntity) {
        userPermissionCacheRedisTemplate.opsForValue().set(String.format(KEY_TEMPLATE, userPermissionCacheEntity.getId()), userPermissionCacheEntity, Duration.ofHours(PERMISSION_CACHE_HOUR));
    }

    @Override
    public void remove(Long uid) {
        userPermissionCacheRedisTemplate.delete(String.format(KEY_TEMPLATE, uid));
    }

    @Override
    public void removeAll() {
        Set<String> keys = userPermissionCacheRedisTemplate.keys(String.format(KEY_TEMPLATE, "*"));
        if (!CollectionUtils.isEmpty(keys)) {
            userPermissionCacheRedisTemplate.delete(keys);
        }
    }

    @Override
    public void removeAll(Long... ids) {
        Set<String> keys = new HashSet<>();
        for (Long id : ids) {
            keys.add(KEY_TEMPLATE + id);
        }
        userPermissionCacheRedisTemplate.delete(keys);
    }

    @Override
    public List<String> getPermissionKey(Long id) {
        return findById(id).getPermissionKeyList();
    }
}
