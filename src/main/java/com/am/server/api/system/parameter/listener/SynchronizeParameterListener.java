package com.am.server.api.system.parameter.listener;

import com.am.server.api.system.parameter.service.SynchronizeParameterService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @author 阮雪峰
 */
@Slf4j
@Component
public class SynchronizeParameterListener implements ApplicationListener<ContextRefreshedEvent> {

    private final SynchronizeParameterService synchronizeParameterService;

    public SynchronizeParameterListener(SynchronizeParameterService synchronizeParameterService) {
        this.synchronizeParameterService = synchronizeParameterService;
    }

    @Override
    public void onApplicationEvent(@NotNull ContextRefreshedEvent event) {
        synchronizeParameterService.load();
    }
}
