package com.am.server.api.system.user.model.mapper;

import com.am.server.api.system.user.model.dto.AdminUserDto;
import com.am.server.api.system.user.model.entity.AdminUserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface AdminUserMapper {

    @Mapping(target = "creatorName", source = "createdBy.username")
    AdminUserDto toDto(AdminUserEntity entity);
}
