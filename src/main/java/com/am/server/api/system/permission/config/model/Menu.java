package com.am.server.api.system.permission.config.model;

import com.am.server.api.system.permission.model.enumerate.MenuType;
import com.am.server.common.util.tree.TreeNode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class Menu implements TreeNode<String, Menu> {
    private String id;
    /**
     * path
     */
    private String path;
    private String fullPath;

    private String redirect;
    /**
     * 名称
     */
    private String name;
    /**
     * 父级id
     */
    private String parentId;
    /**
     * 组件
     */
    private String component;
    /**
     * 菜单类型
     */
    private MenuType type;

    private List<String> key;

    private Meta meta;

    private Boolean hideMenu;

    private List<Menu> children;

    public Menu() {
        type = MenuType.MENU;
        key = new ArrayList<>();
    }

    /**
     * 是否拥有子集
     *
     * @return boolean
     */
    public boolean hasChildren() {
        return children != null && !children.isEmpty();
    }

    /**
     * 是否有key值
     *
     * @return boolean
     */
    public boolean hasKey() {
        return key != null && !key.isEmpty();
    }

}
