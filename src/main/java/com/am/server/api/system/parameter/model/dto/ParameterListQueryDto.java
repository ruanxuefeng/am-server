package com.am.server.api.system.parameter.model.dto;

import com.am.server.api.system.parameter.model.enumerate.ParameterType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class ParameterListQueryDto {
    /**
     * 全名
     */
    private String fullPath;
    /**
     * 唯一key值
     */
    private String uniqueKey;
    /**
     * 类型
     */
    private ParameterType type;
}
