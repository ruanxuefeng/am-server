package com.am.server.api.system.permission.service;

import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.api.system.user.model.entity.UserPermissionCacheEntity;

import java.util.List;
import java.util.Optional;

/**
 * 用户权限缓存
 *
 * @author 阮雪峰
 */
public interface UserPermissionCacheService {
    /**
     * 将用户权限放入缓存中
     *
     * @param uid               用户主键
     * @param permissions       用户权限
     * @param permissionKeyList 权限唯一标识
     * @author 阮雪峰
     */
    void set(Long uid, List<Menu> permissions, List<String> permissionKeyList);

    /**
     * 在缓存中获取用户权限，如果缓存中没有则在数据库中查询，并保存到缓存
     *
     * @param uid 用户主键
     * @return void
     * @author 阮雪峰
     */
    List<Menu> saveOrGet(Long uid);

    /**
     * 只在缓存中查询
     *
     * @param uid uid
     * @return Optional<UserPermissionDo>
     */
    Optional<UserPermissionCacheEntity> get(Long uid);

    /**
     * 删除缓存
     *
     * @param uid uid
     * @author 阮雪峰
     */
    void remove(Long uid);

    /**
     * 清空权限缓存
     *
     * @author 阮雪峰
     */
    void removeAll();

    /**
     * 清除用户权限缓存
     *
     * @param ids 需要清除用户的主键
     */
    void removeAll(Long... ids);

    /**
     * 登录用户的权限key
     *
     * @param loginId  loginId
     * @param loginKey loginKey
     * @return List<String>
     */
    List<String> getPermissionList(Object loginId, String loginKey);

    /**
     * 刷新用户权限缓存
     *
     * @param uid uid
     */
    void refresh(Long uid);
}
