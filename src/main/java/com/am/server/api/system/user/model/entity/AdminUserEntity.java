package com.am.server.api.system.user.model.entity;

import com.am.server.api.system.role.model.entity.RoleEntity;
import com.am.server.common.base.model.entity.BaseEntity;
import com.am.server.common.base.model.enumerate.Gender;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * admin_user
 *
 * @author 阮雪峰
 */
@Table(name = "admin_user")
@Entity
@Setter
@Getter
public class AdminUserEntity extends BaseEntity {
    @Column
    private String username;

    @Column
    private String email;

    @Column(updatable = false)
    private String password;

    @Column(updatable = false)
    private String salt;

    @Column
    private String avatar;

    @Enumerated(EnumType.ORDINAL)
    private Gender gender;

    @Column
    private String homePageId;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @OrderBy("id desc")
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user")},
            inverseJoinColumns = {@JoinColumn(name = "role")})
    @ToString.Exclude
    private List<RoleEntity> roles;

    @Column(name = "created_by")
    private Long createdById;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "created_by", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT), insertable = false, updatable = false)
    private AdminUserEntity createdBy;

    @Column(name = "updated_by")
    private Long updatedById;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "updated_by", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT), insertable = false, updatable = false)
    @ToString.Exclude
    private AdminUserEntity updatedBy;
}
