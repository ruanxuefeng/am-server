package com.am.server.api.system.permission.constant;

/**
 * @author 阮雪峰
 */
public class PermissionLogConstant {
    private PermissionLogConstant() {
    }

    public static final String MENU_NAME = "权限管理";
}
