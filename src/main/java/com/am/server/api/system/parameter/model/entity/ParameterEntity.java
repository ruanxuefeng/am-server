package com.am.server.api.system.parameter.model.entity;

import com.am.server.api.system.parameter.model.enumerate.ParameterType;
import com.am.server.common.base.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author 阮雪峰
 */
@Table(name = "parameter")
@Entity
@Accessors(chain = true)
@Setter
@Getter
public class ParameterEntity extends BaseEntity {
    /**
     * 参数值
     */
    @Column(nullable = false)
    private String value;
    /**
     * 参数名称
     */
    private String name;
    /**
     * 唯一key值
     */
    private String uniqueKey;
    /**
     * 参数目录
     */
    @Column(nullable = false)
    private String catlog;
    /**
     * 类型
     */
    @Enumerated(EnumType.STRING)
    private ParameterType type;
    /**
     * 参数备注
     */
    private String description;
    /**
     * 全名
     */
    private String fullPath;
    /**
     * 在代码中已经删除
     */
    @Column(name = "del")
    private Boolean delete;
}
