package com.am.server.api.system.parameter.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class ParameterDto {
    /**
     * 参数名
     */
    private String name;
    /**
     * 唯一key值
     */
    private String uniqueKey;
    /**
     * 参数值
     */
    private String value;
}
