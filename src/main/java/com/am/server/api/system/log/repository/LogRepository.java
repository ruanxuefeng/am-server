package com.am.server.api.system.log.repository;

import com.am.server.api.system.log.model.entity.LogEntity;
import com.am.server.common.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 阮雪峰
 */
@Repository
public interface LogRepository extends BaseRepository<LogEntity> {
}
