package com.am.server.api.system.parameter.annotation;

import cn.hutool.core.text.CharSequenceUtil;
import com.am.server.api.system.parameter.model.enumerate.ParameterType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 系统参数注解<br>
 * 系统启动时会同步
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemParameter {
    /**
     * 默认值
     *
     * @return String
     */
    String value();

    /**
     * 唯一key
     *
     * @return String
     */
    String uniqueKey();

    /**
     * 参数名称
     *
     * @return String
     */
    String name();

    /**
     * 参数目录
     *
     * @return String
     */
    String catlog();

    /**
     * 类型
     *
     * @return ParameterType
     */
    ParameterType type() default ParameterType.STRING;

    /**
     * 说明
     *
     * @return String
     */
    String description() default CharSequenceUtil.EMPTY;
}
