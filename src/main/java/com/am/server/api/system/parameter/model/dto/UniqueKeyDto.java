package com.am.server.api.system.parameter.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class UniqueKeyDto {
    /**
     * 唯一值
     */
    private String uniqueKey;
}
