package com.am.server.api.system.user.constant;

/**
 * 用户管理权限常量类
 *
 * @author 阮雪峰
 */
public class UserPermissionConstant {
    /**
     * 新增用户
     */
    public static final String PERMISSION_USER_ADD = "system-user-add";
    /**
     * 修改用户信息
     */
    public static final String PERMISSION_USER_UPDATE = "system-user-update";
    /**
     * 删除用户
     */
    public static final String PERMISSION_USER_DELETE = "system-user-delete";
    /**
     * 修改用户权限
     */
    public static final String PERMISSION_USER_RESET_PASSWORD = "system-user-reset-password";
    /**
     * 修改用户角色
     */
    public static final String PERMISSION_USER_CHANGE_ROLE = "system-user-change-role";

    private UserPermissionConstant() {
    }
}
