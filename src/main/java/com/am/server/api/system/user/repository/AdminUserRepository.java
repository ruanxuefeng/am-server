package com.am.server.api.system.user.repository;

import com.am.server.api.system.user.model.entity.AdminUserEntity;
import com.am.server.common.base.repository.BaseRepository;

import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface AdminUserRepository extends BaseRepository<AdminUserEntity> {
    /**
     * 邮箱查询用户
     *
     * @param email email
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByEmail(String email);

    /**
     * 在修改的时候排除现有用户进行是否存在校验
     *
     * @param id    id
     * @param email email
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByIdNotAndEmail(Long id, String email);

    /**
     * 用户名查询用户
     *
     * @param username 用户名
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByUsername(String username);

    /**
     * 在修改的时候排除现有用户进行是否存在校验
     *
     * @param id       id
     * @param username username
     * @return Optional<AdminUserPO>
     * @author 阮雪峰
     */
    Optional<AdminUserEntity> findByIdNotAndUsername(Long id, String username);
}
