package com.am.server.api.system.permission.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author 阮雪锋
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MenuTreeDto {
    /**
     * 主键
     */
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 路由
     */
    private String path;

    /**
     * 子集
     */
    private List<MenuTreeDto> children;
}
