package com.am.server.api.system.dictionary.model.mapper;

import com.am.server.api.system.dictionary.model.dto.DictionaryDto;
import com.am.server.api.system.dictionary.model.dto.SaveDictionaryDto;
import com.am.server.api.system.dictionary.model.entity.DictionaryEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

/**
 * @author 阮雪峰
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DictionaryMapper {
    @Mapping(target = "creatorName", source = "createdBy.username")
    DictionaryDto toDto(DictionaryEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdTime", ignore = true)
    @Mapping(target = "updatedTime", ignore = true)
    @Mapping(target = "revision", ignore = true)
    @Mapping(target = "createdById", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "updatedById", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "items", ignore = true)
    DictionaryEntity saveDtoToEntity(SaveDictionaryDto dto);
}
