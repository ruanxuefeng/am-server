package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.validator.Login;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录
 *
 * @author 阮雪峰
 */
@Data
public class LoginDto {
    /**
     * 用户名
     */
    @NotBlank(message = "login.username.blank", groups = {Login.class})
    private String username;
    /**
     * 密码
     */
    @NotBlank(message = "login.password.blank", groups = {Login.class})
    private String password;

    private Boolean rememberMe;
}
