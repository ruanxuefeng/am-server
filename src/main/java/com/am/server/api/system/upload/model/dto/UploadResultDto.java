package com.am.server.api.system.upload.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 文件上传结果
 *
 * @author 阮雪锋
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UploadResultDto {
    private Long id;
    /**
     * 文件地址
     */
    private String url;
}
