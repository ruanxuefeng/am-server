package com.am.server.api.system.role.model.mapper;

import com.am.server.api.system.role.model.dto.RoleDto;
import com.am.server.api.system.role.model.dto.SaveRoleDto;
import com.am.server.api.system.role.model.dto.SimpleRoleDto;
import com.am.server.api.system.role.model.entity.RoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

/**
 * @author 阮雪峰
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface RoleMapper {
    @Mapping(target = "creatorName", source = "createdBy.username")
    @Mapping(target = "homePageTitle", ignore = true)
    RoleDto toDto(RoleEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdTime", ignore = true)
    @Mapping(target = "updatedTime", ignore = true)
    @Mapping(target = "revision", ignore = true)
    @Mapping(target = "createdById", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "updatedById", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    RoleEntity saveDtoToEntity(SaveRoleDto dto);

    SimpleRoleDto toSimpleDto(RoleEntity entity);
}
