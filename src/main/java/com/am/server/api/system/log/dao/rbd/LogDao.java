package com.am.server.api.system.log.dao.rbd;

import com.am.server.api.system.log.model.dto.LogListQueryDto;
import com.am.server.api.system.log.model.entity.LogEntity;
import org.springframework.data.domain.Page;

/**
 * @author 阮雪峰
 */
public interface LogDao {
    /**
     * 新增
     *
     * @param log log
     */
    void save(LogEntity log);

    /**
     * 分页查询
     *
     * @param logListQueryDto logListAo
     * @return Page<LogDo>
     */
    Page<LogEntity> findAll(LogListQueryDto logListQueryDto);
}
