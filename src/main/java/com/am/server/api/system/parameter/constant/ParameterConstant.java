package com.am.server.api.system.parameter.constant;

/**
 * @author 阮雪峰
 */
public class ParameterConstant {
    private ParameterConstant() {
    }

    /**
     * 系统参数目录分隔符
     */
    public static final String CATLOG_DELIMITER = "-";
}
