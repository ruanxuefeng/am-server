package com.am.server.api.system.role.repository;

import com.am.server.api.system.role.model.entity.RoleEntity;
import com.am.server.common.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 阮雪峰
 */
@Repository
public interface RoleRepository extends BaseRepository<RoleEntity> {

}
