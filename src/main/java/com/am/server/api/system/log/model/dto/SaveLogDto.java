package com.am.server.api.system.log.model.dto;

import com.am.server.api.system.log.model.entity.LogExtendEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Data
public class SaveLogDto {
    /**
     * 操作人
     */
    private String name;

    /**
     * 菜单
     */
    private String menu;

    /**
     * 操作
     */
    private String operate;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 扩展内容
     */
    private LogExtendEntity extend;
}
