package com.am.server.api.system.parameter.constant;

/**
 * @author 阮雪峰
 */
public class ParameterLogConstant {
    private ParameterLogConstant() {
    }

    public static final String MENU_NAME = "参数管理";
    public static final String PARAMETER_UPDATE_METHOD_NAME = "修改";
}
