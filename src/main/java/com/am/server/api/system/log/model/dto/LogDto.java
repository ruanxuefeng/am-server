package com.am.server.api.system.log.model.dto;

import com.am.server.api.system.log.model.entity.LogExtendEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Data
public class LogDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -3692743534427201269L;
    private Long id;
    /**
     * 操作人
     */
    private String name;

    /**
     * 菜单
     */
    private String menu;

    /**
     * 操作
     */
    private String operate;

    /**
     * ip地址
     */
    private String ip;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdTime;

    private LogExtendEntity extend;
}
