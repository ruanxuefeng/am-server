package com.am.server.api.system.user.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 登录用户
 *
 * @author 阮雪峰
 */
@AllArgsConstructor
@Data
public class LoginUserInfoDto {
    /**
     * 登录凭证
     */
    private String token;
}
