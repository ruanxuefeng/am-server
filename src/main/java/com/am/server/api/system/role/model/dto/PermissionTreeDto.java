package com.am.server.api.system.role.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author 阮雪峰
 */
@Data
@Accessors(chain = true)
public class PermissionTreeDto {
    private String name;
    private String mark;
    private List<PermissionTreeDto> children;
}
