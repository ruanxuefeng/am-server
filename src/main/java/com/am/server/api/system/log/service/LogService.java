package com.am.server.api.system.log.service;

import com.am.server.api.system.log.model.dto.LogDto;
import com.am.server.api.system.log.model.dto.LogListQueryDto;
import com.am.server.api.system.log.model.dto.SaveLogDto;
import com.am.server.common.base.model.dto.PageDto;

/**
 * @author 阮雪峰
 */
public interface LogService {
    /**
     * 保存日志
     *
     * @param log log
     * @author 阮雪峰
     */
    void save(SaveLogDto log);

    /**
     * list
     *
     * @param query query
     * @return PageVO<LogListVO>
     * @author 阮雪峰
     */
    PageDto<LogDto> find(LogListQueryDto query);
}
