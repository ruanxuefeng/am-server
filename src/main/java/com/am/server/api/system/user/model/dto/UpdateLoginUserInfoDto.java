package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.model.enumerate.Gender;
import com.am.server.common.constant.RegularConstant;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * 登录用户修改自己的信息
 *
 * @author 阮雪峰
 */
@Accessors(chain = true)
@Data
public class UpdateLoginUserInfoDto {

    /**
     * 主键
     */
    private Long id;

    /**
     * 邮箱
     */
    @NotBlank(message = "user.email.blank")
    @Email(regexp = RegularConstant.EMAIL, message = "user.email.format")
    private String email;

    /**
     * 性别
     */
    private Gender gender;

    /**
     * 头像链接
     */
    private String avatar;

    /**
     * 头像文件
     */
    private MultipartFile img;
}
