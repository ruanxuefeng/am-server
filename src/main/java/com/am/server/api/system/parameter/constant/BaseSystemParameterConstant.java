package com.am.server.api.system.parameter.constant;

import com.am.server.api.system.parameter.annotation.SystemParameter;
import com.am.server.api.system.parameter.annotation.SystemParameterConstant;

/**
 * @author 阮雪峰
 */
@SystemParameterConstant
public class BaseSystemParameterConstant {
    private BaseSystemParameterConstant() {
    }

    @SystemParameter(uniqueKey = "SYSTEM_NAME", value = "AM管理系统", name = "系统名称", catlog = "系统参数-基本参数")
    public static final String SYSTEM_NAME = "SYSTEM_NAME";

    @SystemParameter(uniqueKey = "SYSTEM_NAME1", value = "AM管理系统1", name = "系统名称1", catlog = "系统参数-基本参数")
    public static final String SYSTEM_NAME1 = "SYSTEM_NAME1";

    @SystemParameter(uniqueKey = "test1", value = "test1", name = "test1", catlog = "测试参数-层级1-层级1")
    public static final String TEST_1 = "test1";
    //    @SystemParameter(uniqueKey = "test3", value = "test1", name = "test3", catlog = "测试参数-层级1")
//    public static final String TEST_3 = "test3";
    @SystemParameter(uniqueKey = "test2", value = "test2", name = "test2", catlog = "测试参数-层级1-层级2")
    public static final String TEST_2 = "test2";
}
