package com.am.server.api.system.user.model.dto;

import lombok.Data;

/**
 * 用户名是否被使用
 *
 * @author 阮雪峰
 */
@Data
public class UsernameExistDto {
    /**
     * 用户id(在修改的时候必填)
     */
    private Long id;
    /**
     * 用户名
     *
     * @required
     */
    private String username;
}
