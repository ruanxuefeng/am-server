package com.am.server.api.system.dictionary.model.mapper;

import com.am.server.api.system.dictionary.model.dto.DictionaryItemDto;
import com.am.server.api.system.dictionary.model.dto.SaveDictionaryItemDto;
import com.am.server.api.system.dictionary.model.entity.DictionaryItemEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DictionaryItemMapper {
    @Mapping(target = "creatorName", source = "createdBy.username")
    DictionaryItemDto toDto(DictionaryItemEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdTime", ignore = true)
    @Mapping(target = "updatedTime", ignore = true)
    @Mapping(target = "revision", ignore = true)
    @Mapping(target = "createdById", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "updatedById", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "dictionary", ignore = true)
    DictionaryItemEntity saveDtoToEntity(SaveDictionaryItemDto dto);
}
