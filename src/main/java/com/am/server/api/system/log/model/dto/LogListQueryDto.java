package com.am.server.api.system.log.model.dto;

import com.am.server.common.base.model.dto.PageQueryDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author 阮雪峰
 */
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Data
public class LogListQueryDto extends PageQueryDto {
    /**
     * 操作人姓名
     */
    private String name;

    /**
     * 操作
     */
    private String operate;

    /**
     * 菜单名称
     */
    private String menu;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private LocalDate startDate;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private LocalDate endDate;
}
