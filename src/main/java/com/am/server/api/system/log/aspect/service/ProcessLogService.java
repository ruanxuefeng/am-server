package com.am.server.api.system.log.aspect.service;

import com.am.server.api.system.log.aspect.annotation.WriteLog;
import org.aspectj.lang.JoinPoint;

import java.lang.reflect.Method;

/**
 * 处理日志用户操作日志接口
 *
 * @author 阮雪峰
 */
public interface ProcessLogService {
    /**
     * 处理日志
     *
     * @param targetClass          需要记录日志的类
     * @param targetClassWriteLog  需要记录日志类的WriteLog注解
     * @param targetMethod         需要记录日志类具体执行的方法
     * @param targetMethodWriteLog 方法上的WriteLog注解
     * @param joinPoint            joinPoint
     */
    void process(Class<?> targetClass, WriteLog targetClassWriteLog, Method targetMethod, WriteLog targetMethodWriteLog, JoinPoint joinPoint);
}
