package com.am.server.api.system.permission.dao.cache;

import com.am.server.api.system.permission.model.entity.SystemPermissionCacheEntity;

import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface SystemPermissionCacheDao {
    /**
     * save all
     *
     * @param systemPermissionCacheEntity permissionCacheDo
     */
    void save(SystemPermissionCacheEntity systemPermissionCacheEntity);

    /**
     * 查询系统权限数据
     *
     * @return List<Menu>
     */
    Optional<SystemPermissionCacheEntity> get();
}
