package com.am.server.api.system.user.service;

import com.am.server.api.system.permission.config.model.Menu;
import com.am.server.api.system.user.model.dto.LoginDto;
import com.am.server.api.system.user.model.dto.LoginUserInfoDto;
import com.am.server.api.system.user.model.dto.UpdateLoginUserInfoDto;
import com.am.server.api.system.user.model.dto.UserInfoDto;

import java.util.List;

/**
 * @author 阮雪峰
 */
public interface LoginUserService {
    /**
     * 登录
     *
     * @param dto 用户信息
     * @return UserInfoVo
     */
    LoginUserInfoDto login(LoginDto dto);

    /**
     * 获取用户信息
     *
     * @param id 用户信息
     * @return UserInfoVo
     */
    UserInfoDto info(Long id);

    /**
     * 用户修改自己的基本信息
     *
     * @param user 用户信息
     */
    void update(UpdateLoginUserInfoDto user);

    /**
     * 获取当前登录用户权限菜单
     *
     * @return List<Menu>
     */
    List<Menu> getMenuList();

    /**
     * 退出登录
     */
    void logout();

    /**
     * 刷新用户权限
     *
     * @param uid 用户id
     */
    void refreshUserPermission(Long uid);
}
