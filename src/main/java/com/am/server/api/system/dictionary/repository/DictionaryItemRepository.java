package com.am.server.api.system.dictionary.repository;

import com.am.server.api.system.dictionary.model.entity.DictionaryItemEntity;
import com.am.server.common.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 阮雪峰
 */
@Repository
public interface DictionaryItemRepository extends BaseRepository<DictionaryItemEntity> {
}
