package com.am.server.api.system.upload.exception;

import com.am.server.common.base.exception.BusinessException;
import org.springframework.http.HttpStatus;

import java.io.IOException;

/**
 * 文件上传失败
 *
 * @author 阮雪峰
 */
public class DownloadFileException extends BusinessException {

    public DownloadFileException(String message, IOException e) {
        super(message, e, HttpStatus.BAD_REQUEST);
    }
}
