package com.am.server.api.system.dictionary.dao.rdb;

import com.am.server.api.system.dictionary.model.dto.DictionaryListQueryDto;
import com.am.server.api.system.dictionary.model.entity.DictionaryEntity;
import org.springframework.data.domain.Page;

import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface DictionaryDao {
    /**
     * 分页查询
     *
     * @param dto dto
     * @return Page<DictionaryEntity>
     */
    Page<DictionaryEntity> findAll(DictionaryListQueryDto dto);

    /**
     * 保存
     *
     * @param entity entity
     */
    void save(DictionaryEntity entity);

    /**
     * 删除
     *
     * @param id 主键
     */
    void deleteById(Long id);

    /**
     * 根据编码查询
     *
     * @param code code
     * @return Optional<DictionaryEntity>
     */
    Optional<DictionaryEntity> findByCode(String code);


    /**
     * 排除当前数据，判断编码是否存在
     *
     * @param code code
     * @param id   id
     * @return Optional<DictionaryEntity>
     */
    Optional<DictionaryEntity> findByCodeWithId(String code, Long id);

    /**
     * 根据id查询
     *
     * @param id id
     * @return Optional<DictionaryEntity>
     */
    Optional<DictionaryEntity> findById(Long id);
}
