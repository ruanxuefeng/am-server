package com.am.server.api.system.user.model.dto;

import com.am.server.common.base.model.dto.BaseListDto;
import com.am.server.common.base.model.enumerate.Gender;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serial;


/**
 * @author 阮雪峰
 */
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Accessors(chain = true)
@Data
public class AdminUserDto extends BaseListDto {

    @Serial
    private static final long serialVersionUID = 7269426912451758213L;
    /**
     * 用户名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别
     */
    private Gender gender;

    private String homePageId;
}
