package com.am.server.api.system.log.model.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class DefaultLogExtendEntity implements LogExtendEntity {
    /**
     * 操作日志参数
     */
    private String parameter;

    public DefaultLogExtendEntity() {
    }

    public DefaultLogExtendEntity(final String parameter) {
        this.parameter = parameter;
    }

}
