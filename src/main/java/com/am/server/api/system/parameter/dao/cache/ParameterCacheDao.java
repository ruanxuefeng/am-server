package com.am.server.api.system.parameter.dao.cache;

import com.am.server.api.system.parameter.model.entity.ParameterCacheEntity;
import com.am.server.api.system.parameter.model.entity.ParameterEntity;

import java.util.Collection;
import java.util.List;

public interface ParameterCacheDao {
    /**
     * 保存到缓存
     *
     * @param parameter parameter
     */
    void save(ParameterEntity parameter);

    /**
     * 保存到缓存
     *
     * @param parameter parameter
     */
    void save(ParameterCacheEntity parameter);

    /**
     * 保存全部
     *
     * @param parameters parameters
     */
    void saveAll(List<ParameterEntity> parameters);

    /**
     * 获取缓存数据
     *
     * @param id id
     * @return ParameterCacheDo
     */
    ParameterCacheEntity get(Long id);

    /**
     * 查询所有
     *
     * @param ids ids
     * @return List<ParameterCacheDo>
     */
    List<ParameterCacheEntity> getAll(Collection<Long> ids);

    /**
     * 查询所有参数
     *
     * @return List<ParameterCacheDo>
     */
    List<ParameterCacheEntity> getAll();

    /**
     * 删除缓存
     *
     * @param id id
     */
    void remove(Long id);

    /**
     * 移除对应缓存
     *
     * @param ids ids
     */
    void clear(Collection<Long> ids);

    /**
     * 移除所有参数缓存
     */
    void clear();
}
