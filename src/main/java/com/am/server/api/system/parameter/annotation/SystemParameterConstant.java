package com.am.server.api.system.parameter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * <li>系统参数常量注解</li>
 * <li>系统启动时会扫描使用此注解的类，找到使用@Parameter注解的常量同步至数据库</li>
 */
@Target({ElementType.TYPE})
public @interface SystemParameterConstant {
}
