package com.am.server.api.system.parameter.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author 阮雪峰
 */
@Setter
@Getter
public class UpdateParameterDto {
    /**
     * 参数key
     */
    @NotBlank(message = "parameter.uniqueKey.blank")
    private String uniqueKey;
    /**
     * 参数值
     */
    private String value;
}
