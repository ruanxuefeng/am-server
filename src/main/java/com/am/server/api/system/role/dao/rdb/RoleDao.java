package com.am.server.api.system.role.dao.rdb;

import com.am.server.api.system.role.model.dto.RoleListQueryDto;
import com.am.server.api.system.role.model.entity.RoleEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
public interface RoleDao {
    /**
     * 分页
     *
     * @param roleListQueryDto roleListAo
     * @return Page<RoleDo>
     */
    Page<RoleEntity> findAll(RoleListQueryDto roleListQueryDto);

    /**
     * save
     *
     * @param role role
     */
    void save(RoleEntity role);

    /**
     * find by id
     *
     * @param id id
     * @return Optional<RoleDo>
     */
    Optional<RoleEntity> findById(Long id);

    /**
     * delete by id
     *
     * @param id id
     */
    void deleteById(Long id);

    /**
     * find all order by id desc
     *
     * @return List<RoleDo>
     */
    List<RoleEntity> findAllOrderByIdDesc();

    /**
     * find all by ids
     *
     * @param roleIdList roleIdList
     * @return List<RoleDo>
     */
    List<RoleEntity> findAllById(List<Long> roleIdList);
}
