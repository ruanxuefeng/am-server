package com.am.server.api.system.parameter.model.enumerate;

/**
 * @author 阮雪峰
 */
public enum ParameterType {
    /**
     * 整数
     */
    INTEGER("整数"),
    /**
     * 浮点型
     */
    DOUBLE("浮点型"),
    /**
     * 数字
     */
    NUMBER("数字"),
    /**
     * 字符串
     */
    STRING("字符串"),
    /**
     * Json
     */
    JSON("Json"),
    /**
     * 用户
     */
    USER("用户");

    private final String name;

    ParameterType(String name) {
        this.name = name;
    }
}
