package com.am.server.api.system.parameter.service;

import com.am.server.api.system.parameter.model.dto.ParameterDto;
import com.am.server.api.system.parameter.model.dto.ParameterListQueryDto;
import com.am.server.api.system.parameter.model.dto.ParameterTreeDto;
import com.am.server.api.system.parameter.model.dto.UpdateParameterDto;

import java.util.List;

/**
 * @author 阮雪峰
 */
public interface ParameterService {
    /**
     * 树形参数
     *
     * @return List<ParameterTreeVo>
     */
    List<ParameterTreeDto> treeList(ParameterListQueryDto query);

    /**
     * 更新值
     *
     * @param dto dto
     */
    void update(UpdateParameterDto dto);

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKey uniqueKey
     * @return ParameterVo
     */
    ParameterDto getByUniqueKey(String uniqueKey);

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKeys uniqueKeys
     * @return ParameterVo
     */
    List<ParameterDto> getByUniqueKeys(List<String> uniqueKeys);
}
