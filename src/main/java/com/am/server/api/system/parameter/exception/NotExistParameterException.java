package com.am.server.api.system.parameter.exception;

import com.am.server.common.base.exception.BusinessException;
import org.springframework.http.HttpStatus;

/**
 * @author 阮雪峰
 */
public class NotExistParameterException extends BusinessException {
    public NotExistParameterException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
