package com.am.server.api.system.role.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 阮雪峰
 */
@Data
@Accessors(chain = true)
public class UpdateRolePermissionDto {
    /**
     * id
     */
    @NotNull(message = "common.operate.primaryKey.null")
    private Long id;

    /**
     * 权限关键字
     */
    private List<String> permissions;
}
