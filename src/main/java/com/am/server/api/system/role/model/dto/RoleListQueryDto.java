package com.am.server.api.system.role.model.dto;

import com.am.server.common.base.model.dto.PageQueryDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * list
 *
 * @author 阮雪峰
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleListQueryDto extends PageQueryDto {
    /**
     * 角色名称
     */
    private String name;
}
