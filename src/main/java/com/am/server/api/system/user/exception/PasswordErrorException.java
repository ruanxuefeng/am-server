package com.am.server.api.system.user.exception;

import com.am.server.common.base.exception.BusinessException;
import com.am.server.common.constant.MessageConstant;
import org.springframework.http.HttpStatus;

/**
 * 密码不正确
 *
 * @author 阮雪峰
 */
public class PasswordErrorException extends BusinessException {
    public PasswordErrorException() {
        super(MessageConstant.VALIDATE_FAIL, HttpStatus.NOT_ACCEPTABLE);
    }
}
