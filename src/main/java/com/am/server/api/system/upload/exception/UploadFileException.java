package com.am.server.api.system.upload.exception;

import com.am.server.common.base.exception.BusinessException;
import com.am.server.common.constant.MessageConstant;

import java.io.IOException;

/**
 * 文件上传失败
 *
 * @author 阮雪峰
 */
public class UploadFileException extends BusinessException {

    public UploadFileException(IOException e) {
        super(MessageConstant.FILE_UPLOAD_FAIL, e, 521);
    }
}
