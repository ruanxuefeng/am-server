package com.am.server.api.system.parameter.exception;

/**
 * @author 阮雪峰
 */
public class DuplicateParameterException extends RuntimeException {
    public DuplicateParameterException(String uniqueKey, String className1, String className2) {
        super(String.format("在[%s,%s]存在重复的参数[%s]", className1, className2, uniqueKey));
    }
}
