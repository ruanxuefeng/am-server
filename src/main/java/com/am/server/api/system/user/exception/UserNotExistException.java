package com.am.server.api.system.user.exception;

import com.am.server.common.base.exception.BusinessException;
import com.am.server.common.constant.MessageConstant;
import org.springframework.http.HttpStatus;

/**
 * 用户不存在
 *
 * @author 阮雪峰
 */
public class UserNotExistException extends BusinessException {
    public UserNotExistException() {
        super(MessageConstant.VALIDATE_FAIL, HttpStatus.NOT_ACCEPTABLE);
    }
}
