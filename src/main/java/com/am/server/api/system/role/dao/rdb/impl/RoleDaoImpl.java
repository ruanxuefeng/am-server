package com.am.server.api.system.role.dao.rdb.impl;

import com.am.server.api.system.role.dao.rdb.RoleDao;
import com.am.server.api.system.role.model.dto.RoleListQueryDto;
import com.am.server.api.system.role.model.entity.RoleEntity;
import com.am.server.api.system.role.repository.RoleRepository;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author 阮雪峰
 */
@Service
public class RoleDaoImpl implements RoleDao {
    private final RoleRepository roleRepository;

    public RoleDaoImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Page<RoleEntity> findAll(RoleListQueryDto query) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreNullValues()
                .withNullHandler(ExampleMatcher.NullHandler.IGNORE)
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains());
        Example<RoleEntity> example = Example.of(new RoleEntity().setName(query.getName()), matcher);

        return roleRepository.findAll(example, PageRequest.of(query.getPage() - 1, query.getPageSize()));
    }

    @Override
    public void save(RoleEntity role) {
        roleRepository.save(role);
    }

    @Override
    public Optional<RoleEntity> findById(Long id) {
        return roleRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        roleRepository.deleteById(id);
    }

    @Override
    public List<RoleEntity> findAllOrderByIdDesc() {
        return roleRepository.findAll(Sort.by(Sort.Order.desc("id")));
    }

    @Override
    public List<RoleEntity> findAllById(List<Long> roleIdList) {
        return roleRepository.findAllById(roleIdList);
    }
}
