insert into sys_file (id, type, url, dir, revision, created_by, created_time, updated_by, updated_time)
values (1329977624344268800, 0, 'http://localhost:9527/file/avatar/1329977624344268800.jpg?1605926350094',
        '/avatar/1329977624344268800.jpg', 0, null, '2020-11-21 10:39:10', null, '2020-11-21 10:39:10');

insert into admin_user (id, username, avatar, email, gender, salt, name, password, created_time, created_by, updated_by,
                        updated_time, revision)
values (940823560740409344, 'admin', 'http://localhost:9527/file/avatar/1329977624344268800.jpg?1605926350094',
        'admin@am.com', 0, 'iiPfy5J/nhOGL/T7CDTpRoNioUrguqH0', 'admin', 'xtnsaE1hdTA=', null, null, 940823560740409344,
        '2020-08-18 12:22:50', 55);
insert into admin_user (id, username, avatar, email, gender, salt, name, password, created_time, created_by, updated_by,
                        updated_time, revision)
values (1266919488381652992, 'lisi', 'http://localhost:9527/file/avatar/1329977624344268800.jpg?1605926350094',
        'lisi@am.com', null, 'hbDCHK5rzq0=', '李四', 'D2vWvcel2XQ=', '2020-11-21 10:39:10', 940823560740409344,
        940823560740409344, '2020-11-21 10:39:10', 0);

insert into role (id, memo, name, created_by, created_time, updated_by, updated_time, revision)
values (940823091829805056, '超级管理员', '超级管理员', null, '2020-04-05 05:10:47', 940823560740409344, '2020-06-09 13:46:01',
        26);

insert into user_role (role, user)
values (940823091829805056, 940823560740409344);

insert into role_permission (role, permission)
values (940823091829805056, 'system');
insert into role_permission (role, permission)
values (940823091829805056, 'system-user');
insert into role_permission (role, permission)
values (940823091829805056, 'system-user-add');
insert into role_permission (role, permission)
values (940823091829805056, 'system-user-list');
insert into role_permission (role, permission)
values (940823091829805056, 'system-user-update');
insert into role_permission (role, permission)
values (940823091829805056, 'system-role');
insert into role_permission (role, permission)
values (940823091829805056, 'system-scheduled-task');
insert into role_permission (role, permission)
values (940823091829805056, 'log');
insert into role_permission (role, permission)
values (940823091829805056, 'log-operate');
insert into role_permission (role, permission)
values (940823091829805056, 'bulletin');